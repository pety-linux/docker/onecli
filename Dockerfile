FROM golang:1.13 AS downloader

ARG ONECLI_VERSION="v1.22.0.2021.04.30"
ENV ONECLI_FUNCTION="ONECLI" \
    ONECLI_HOME="/oneclidir" \
    ONECLI_DOWNLOAD_URL="https://download.lenovo.com/servers/mig/2021/03/17/43643" \
	ONECLI_TGZ="lnvgy_utl_lxce_onecli02f-3.1.0_rhel_x86-64.tgz" \
	KUBECTL_DOWNLOAD_URL="https://storage.googleapis.com/kubernetes-release/release/v1.20.0/bin/linux/amd64/kubectl"

WORKDIR ${ONECLI_HOME}

COPY ./oneclirest/oneclirest-tls22.go /tmp/oneclirest.go
COPY ./onecliclnt/main/onecliclnt.go /tmp/onecliclnt.go
COPY ./onecliclnt/restclient/restclient.go /tmp/restclient.go
	
RUN	mkdir -p $GOPATH/src/github.com/onmetal/oneclirest && \
	cp /tmp/oneclirest.go $GOPATH/src/github.com/onmetal/oneclirest/oneclirest.go && \
	cd $GOPATH/src/github.com/onmetal/oneclirest && \
	go get github.com/google/uuid && \
	go get github.com/gorilla/mux && \
	go get github.com/boltdb/bolt/... && \
	go build oneclirest.go && \
	cp oneclirest ${ONECLI_HOME}/oneclirest
	
RUN	mkdir -p $GOPATH/src/github.com/onmetal/onecliclnt && \
    mkdir -p $GOPATH/src/github.com/onmetal/onecliclnt/main && \
    mkdir -p $GOPATH/src/github.com/onmetal/onecliclnt/restclient && \	
	cp /tmp/onecliclnt.go $GOPATH/src/github.com/onmetal/onecliclnt/main/onecliclnt.go && \
	cp /tmp/restclient.go $GOPATH/src/github.com/onmetal/onecliclnt/restclient/restclient.go && \
	cd $GOPATH/src/github.com/onmetal/onecliclnt/main && \
	go build onecliclnt.go && \
	cp onecliclnt ${ONECLI_HOME}/onecliclnt

RUN	mkdir -p ${ONECLI_HOME} && \
	cd ${ONECLI_HOME} && \
    curl -SLO "${ONECLI_DOWNLOAD_URL}/${ONECLI_TGZ}" && \
    tar -xzf ${ONECLI_TGZ} && \
	rm ${ONECLI_TGZ}
	
RUN cd ${ONECLI_HOME} && \
    curl -LO ${KUBECTL_DOWNLOAD_URL}
	
FROM debian:stable-slim

ARG ONECLI_VERSION="v1.22.0.2021.04.30"
ENV ONECLI_HOME="/oneclidir" 
ENV ONECLI_FUNCTION="ONECLI"

COPY --from=downloader ${ONECLI_HOME} ${ONECLI_HOME}
COPY ./sh/entrypoint.sh /
COPY ./sh/oneclirest-credentials-example.json /
COPY ./xml/common_result.xml /
COPY ./xml/Onecli-update-compare.xml /
COPY ./xml/Onecli-update-query.xml /
COPY ./xml/Onecli-update-scan.xml /

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y rpm && \
    apt-get install -y jq && \
    apt-get install -y iputils-ping && \
    groupadd -r onecliusr && \
    useradd --no-log-init -r -g onecliusr onecliusr && \
    mkdir -p ${ONECLI_HOME}/pool && \
    mkdir -p ${ONECLI_HOME}/logs && \	
    chown -R onecliusr:onecliusr ${ONECLI_HOME} && \
    mkdir /simulator && \
    mv /common_result.xml /simulator/common_result.xml && \
    mv /Onecli-update-compare.xml /simulator/Onecli-update-compare.xml && \
    mv /Onecli-update-query.xml /simulator/Onecli-update-query.xml  && \
    mv /Onecli-update-scan.xml /simulator/Onecli-update-scan.xml && \
    chown -R onecliusr:onecliusr /simulator && \
    chmod +x "${ONECLI_HOME}/oneclirest" && \
    chmod +x "${ONECLI_HOME}/onecliclnt" && \
    chmod +x "${ONECLI_HOME}/kubectl" && \
    mv "${ONECLI_HOME}/kubectl" /usr/local/bin/kubectl && \
    chmod +x /entrypoint.sh

VOLUME ["/oneclidir/logs", "/oneclidir/pool"]

WORKDIR ${ONECLI_HOME}

EXPOSE 8443

#USER onecliusr
USER root

ENV PATH="${PATH}:/oneclidir"

#ENTRYPOINT ["/bin/bash"]
#CMD ["-c", "while true; do sleep 3600; done"]
ENTRYPOINT ["/entrypoint.sh"]

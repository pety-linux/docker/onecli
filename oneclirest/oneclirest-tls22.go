//
// v1.22.0.2021.06.18
//
package main

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
    "crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
    "encoding/hex"
	"encoding/pem"
	"encoding/json"
    "encoding/xml"	
	"errors"
	"flag"
    "fmt"
	"io"
	"io/ioutil"
    "log"
	"math/big"
	"net"
    "net/http"
	"os"
    "os/exec"
	"regexp"
	"sort"
	"strings"
	"sync"
	"strconv"
	"time"
	
	"github.com/boltdb/bolt"
	"github.com/gorilla/mux"
	guuid "github.com/google/uuid"	
)

/***************/
/*             */
/* sort uint64 */
/*             */
/***************/

type uint64arr []uint64

func (a uint64arr) Len() int           { return len(a) }
func (a uint64arr) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a uint64arr) Less(i, j int) bool { return a[i] < a[j] }


/*
 * Status Created, Pending, Running, Completed, Failed
 */

const ONECLI_STATUS_CREATED		= "Created"
const ONECLI_STATUS_PENDING		= "Pending"
const ONECLI_STATUS_RUNNING		= "Running"
const ONECLI_STATUS_COMPLETED	= "Completed"
const ONECLI_STATUS_FAILED		= "Faileded"

/*
 * Parameters Certificate Creation
 */
const (
	host       = "localhost" 			// Comma-separated hostnames and IPs to generate a certificate for (first in list used for CN)
	validFor   = 10*365*24*time.Hour 	// Duration that certificate is valid for
	isCA       = true 					// whether this cert should be its own Certificate Authority
	rsaBits    = 2048 					// Size of RSA key to generate. Ignored if --ecdsa-curve is set
	ecdsaCurve = "P521" 				// ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521
	ed25519Key = false 					// Generate an Ed25519 key
)

/*
 * JSON struct of file actions pipeline (needed for GET, POST, PATCH)
 */
type OnecliAction struct {
    UUID 			string `json:"uuid"`
	Command 		string `json:"command"`
	Subcommand 		string `json:"subcommand"`
	MachineType 	string `json:"machinetype"`
	OsType 			string `json:"ostype"`
	Scope 			string `json:"scope"`
	User 			string `json:"user"`
	Password		string `json:"password"`
	IP	 			string `json:"ip"`
	Updateid		string `json:"updateid"`
	RebootAllowed	string `json:"rebootallowed"`
	Status 			string `json:"status"`
	Timestamp		string `json:"timestamp"`
}

/*
 * special struct with key for boltdb
 */
type BoltdbAction struct {
	Sequence		uint64  		`json:"sequence"`
	Onecliaction	OnecliAction	`json:"onecliaction"`
}


/*
 * JSON struct of file actions pipeline (needed for GET, POST, PATCH)
 */
type OnecliStatus struct {
	Status 			string `json:"status"`
    Created 		string `json:"created"`
	Pending	 		string `json:"pending"`
	Running 		string `json:"running"`
	Failed 			string `json:"failed"`
	Completed	 	string `json:"completed"`
	All 			string `json:"all"`
}


/*
 * XML struct of file common_result.xml
 */
type CommonResult struct {
	XMLName	xml.Name `xml:"COMMONRESULT"`
	Modules			Modules		`xml:"MODULES"`
	Error			string		`xml:"ERROR"`
	Message			string		`xml:"MESSAGE"`
	Onecli			string		`xml:"ONECLI"`
	Logfile			string		`xml:"LOGFILE"`
}

type Modules struct {
	XMLName			xml.Name	`xml:"MODULES"`
	Modules			[]Module	`xml:"MODULE"`
}

type Module struct {
	XMLName			xml.Name	`xml:"MODULE"`
	Name			string		`xml:"NAME"`
	Returncode		string		`xml:"RETURNCODE"`
	Errormessage	string		`xml:"ERRORMESSAGE"`
}

/*
 * JSON struct of file common_result.xml
 */
type CommonResultJson struct {
	Modules			ModulesJson	`json:"modules"`
	Error			string		`json:"error"`
	Message			string		`json:"message"`
	Onecli			string		`json:"onecli"`
	Logfile			string		`json:"logfile"`
}

type ModulesJson struct {
	Modules			[]ModuleJson	`json:"module"`
}

type ModuleJson struct {
	Name			string		`json:"name"`
	Returncode		string		`json:"returncode"`
	Errormessage	string		`json:"errormessage"`
}

/*
 * XML struct of file Onecli-update-compare.xml
 */
type Comparison struct {
	XMLName	xml.Name `xml:"COMPARISON"`
	Xmlversion		string		`xml:"XMLVERSION"`
	Content			Content		`xml:"CONTENT"`
}

type Content struct {
	XMLName			xml.Name	`xml:"CONTENT"`
	Mt				string		`xml:"MT"`
	Bmctype			string		`xml:"BMCTYPE"`
	Os				string		`xml:"OS"`
	Arch			string		`xml:"ARCH"`
	Scope			string		`xml:"SCOPE"`
	Type			string		`xml:"TYPE"`
	Directory		string		`xml:"DIRETORY"`
	Ispartition		string		`xml:"ISPARTITION"`
	Total			string		`xml:"TOTAL"`
	Packages		Packages	`xml:"PACKAGES"`
}

type Packages struct {
	XMLName			xml.Name	`xml:"PACKAGES"`
	Packages		[]Package	`xml:"PACKAGE"`
}

type Package struct {
	XMLName			xml.Name	`xml:"PACKAGE"`
	Name			string		`xml:"NAME"`
	Component		string		`xml:"COMPONENT"`
	Severity		string		`xml:"SEVERITY"`
	Severitydes		string		`xml:"SEVERITYDES"`
	Reboot			string		`xml:"REBOOT"`
	Rebootdes		string		`xml:"REBOOTDES"`
	Updateid		string		`xml:"UPDATEID"`
	Pkgversion		string		`xml:"PKGVERSION"`
	Requisite		string		`xml:"REQUISITE"`
	Prerequisite	string		`xml:"PREREQUISITE"`
	Corequisite		string		`xml:"COREQUISITE"`
	Packagexml		string		`xml:"PACKAGEXML"`
	Uxspxml			string		`xml:"UXSPXML"`
	Payloadfile		string		`xml:"PAYLOADFILE"`
	Commandline		string		`xml:"COMMANDLINE"`
	Childupdates	Childupdates `xml:"CHILDUPDATES"`
	Cinstalled		string		`xml:"CINSTALLED"`	
	Alsoselect		string		`xml:"ALSOSELECT"`
	Alsodesselect	string		`xml:"ALSODESELECT"`
	Selected		string		`xml:"SELECTED"`
	Forceid			string		`xml:"FORCEID"`
	Compresult		string		`xml:"COMPRESULT"`
	Supersede		string		`xml:"SUPERSEDE"`
	Unselectedreason string		`xml:"UNSELECTEDREASON"`
	Flashorder		string		`xml:"FLASHORDER"`
	Agentlesssuport	string		`xml:"AGENTLESSSUPPORT"`
}

type Childupdates struct {
	XMLName			xml.Name	`xml:"CHILDUPDATES"`
	Childupdates	[]Childupdate `xml:"CHILDUPDATE"`
}

type Childupdate struct {
	XMLName			xml.Name	`xml:"CHILDUPDATE"`
	Cadaptername	string		`xml:"CADAPTERNAME"`
	Cname			string		`xml:"CNAME"`
	Ccategory		string		`xml:"CCATEGORY"`
	Cinstanceid		string		`xml:"CINSTANCEID"`
	Cnewversion		string		`xml:"CNEWVERSION"`
	Cinstalled		string		`xml:"CINSTALLED"`
	Cdescription	string		`xml:"CDESCRIPTION"`
	Cslotnum		string		`xml:"CSLOTNUM"`
	Cslottype		string		`xml:"CSLOTTYPE"`
	Cclassification	string		`xml:"CCLASSIFICATION"`
	Ccompareresult	string		`xml:"CCOMPARERESULT"`
	Cselected		string		`xml:"CSELECTED"`
}

/*
 * JSON struct of file Onecli-update-compare.xml
 */
type RootJson struct {
	Error			string		`json:"ERROR"`
	Message			string		`json:"MESSAGE"`
	Logfile			string		`json:"LOGFILE"`
	Comparison		ComparisonJson	`json:"COMPARISON"`	
}

type ComparisonJson struct {
	Content			ContentJson	`json:"CONTENT"`
}

type ContentJson struct {
	Mt				string		`json:"MT"`
	Bmctype			string		`json:"BMCTYPE"`
	Os				string		`json:"OS"`
	Arch			string		`json:"ARCH"`
	Scope			string		`json:"SCOPE"`
	Type			string		`json:"TYPE"`
	Directory		string		`json:"DIRETORY"`
	Ispartition		string		`json:"ISPARTITION"`
	Total			string		`json:"TOTAL"`
	Packages		[]PackageJson `json:"PACKAGES"`
}

type PackageJson struct {
	Name			string		`json:"NAME"`
	Component		string		`json:"COMPONENT"`
	Severity		string		`json:"SEVERITY"`
	Severitydes		string		`json:"SEVERITYDES"`
	Reboot			string		`json:"REBOOT"`
	Rebootdes		string		`json:"REBOOTDES"`
	Updateid		string		`json:"UPDATEID"`
	Pkgversion		string		`json:"PKGVERSION"`
	Requisite		string		`json:"REQUISITE"`
	Prerequisite	string		`json:"PREREQUISITE"`
	Corequisite		string		`json:"COREQUISITE"`
	Packagexml		string		`json:"PACKAGEXML"`
	Uxspxml			string		`json:"UXSPXML"`
	Payloadfile		string		`json:"PAYLOADFILE"`
	Commandline		string		`json:"COMMANDLINE"`
	Childupdates	[]ChildupdateJson `json:"CHILDUPDATES"`
	Cinstalled		string		`json:"CINSTALLED"`
	Alsoselect		string		`json:"ALSOSELECT"`
	Alsodesselect	string		`json:"ALSODESELECT"`
	Selected		string		`json:"SELECTED"`
	Forceid			string		`json:"FORCEID"`
	Compresult		string		`json:"COMPRESULT"`
	Supersede		string		`json:"SUPERSEDE"`
	Unselectedreason string		`json:"UNSELECTEDREASON"`
	Flashorder		string		`json:"FLASHORDER"`
	Agentlesssuport	string		`json:"AGENTLESSSUPPORT"`
}

type ChildupdateJson struct {
	Cadaptername	string		`json:"CADAPTERNAME"`
	Cname			string		`json:"CNAME"`
	Ccategory		string		`json:"CCATEGORY"`
	Cinstanceid		string		`json:"CINSTANCEID"`
	Cnewversion		string		`json:"CNEWVERSION"`
	Cinstalled		string		`json:"CINSTALLED"`
	Cdescription	string		`json:"CDESCRIPTION"`
	Cslotnum		string		`json:"CSLOTNUM"`
	Cslottype		string		`json:"CSLOTTYPE"`
	Cclassification	string		`json:"CCLASSIFICATION"`
	Ccompareresult	string		`json:"CCOMPARERESULT"`
	Cselected		string		`json:"CSELECTED"`
}

/*
 * OnecliApi struct
 */
type OnecliApi struct {
	sync.Mutex
	simulator		bool
	oneclidir		string
	pooldir			string
	logsdir			string
	OnecliActions 	[]OnecliAction
	db 				*bolt.DB
	bucketName		[]byte
	username		string
	password		string
	certfile		string
	keyfile			string
}

//
// func opendb opens the key-value store (boltdb)
//
func (oa *OnecliApi) opendb() error {

	var err error
	var dbpath string
	
	err = nil
	
	oa.bucketName = []byte("oneclirest")
	dbpath = oa.logsdir + "/db"
	err = oa.makeDirectoryIfNotExists(dbpath)
	if err != nil {
		log.Println("initdb: could not create db directory")
		return err
	}

	dbpath = oa.logsdir + "/db/oneclirest.db"	
	// Open the my.db data file in the current directory.
	// It will be created if it doesn't exist.
	oa.db, err = bolt.Open(dbpath, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Printf ("error func opendb() %s\n",err.Error())
		return err
	}
	
	err = oa.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(oa.bucketName)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		log.Printf ("error func opendb() %s\n",err.Error())
		return err
	}
		
	return err
}

//
// func closedb closes the key-value store (boltdb)
//
func (oa *OnecliApi) closedb() error {
	err := oa.db.Close()
	if err != nil {
		return err
	}
	return err
}

//
// func dbempty checks if the key-value store (boltdb) is empty
//
func (oa *OnecliApi) dbempty() bool {

	var sequence uint64
	
	sequence = 0

	oa.Lock()
	defer oa.Unlock()

	err := oa.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(oa.bucketName)
		/*
		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			sequence++
		}
		*/	
		sequence = b.Sequence()
		return nil
	})
	
	if err != nil {
		return false
	}
	
	if sequence > 0 {
		return false
	}
	return true
}

//
// func loadexamples lodas some examples to the key-value store
// status is "Completed", it means no action will be started
//
func (oa *OnecliApi) loadexamples () {

	var InitOnecliActions []OnecliAction

    InitOnecliActions = []OnecliAction{
        OnecliAction{UUID: "0c965d66-25db-4c57-9d91-0d39213ef0eb", Command: "update", Subcommand: "acquire", MachineType: "7x02", OsType: "none",     Scope: "latest", Status: "Completed", Timestamp: "2020-01-10T16:06:34Z" },
        OnecliAction{UUID: "20d08b6e-7fe0-4530-8332-09b1d5f0c1b4", Command: "update", Subcommand: "acquire", MachineType: "7x02", OsType: "platform", Scope: "latest", Status: "Completed", Timestamp: "2020-01-11T16:06:34Z" },
        OnecliAction{UUID: "4150b4c5-a76a-4b66-881b-351a7246a63d", Command: "update", Subcommand: "acquire", MachineType: "7x21", OsType: "none",     Scope: "latest", Status: "Completed", Timestamp: "2020-01-12T16:06:34Z" },
        OnecliAction{UUID: "4220fc8c-21b6-47fb-9a10-0cfd910d7c97", Command: "update", Subcommand: "acquire", MachineType: "7x21", OsType: "platform", Scope: "latest", Status: "Completed", Timestamp: "2020-01-13T16:06:34Z" },
        OnecliAction{UUID: "42d59f47-a993-4ffb-b8a4-e5f93f6bd770", Command: "update", Subcommand: "acquire", MachineType: "7x20", OsType: "none",     Scope: "latest", Status: "Completed", Timestamp: "2020-01-14T16:06:34Z" },
        OnecliAction{UUID: "8597f625-9702-4ba6-a3e5-586814bf86a8", Command: "update", Subcommand: "acquire", MachineType: "7x20", OsType: "platform", Scope: "latest", Status: "Completed", Timestamp: "2020-01-15T16:06:34Z" },
        OnecliAction{UUID: "89ed6ce7-211f-4e21-a19f-104983bae897", Command: "update", Subcommand: "acquire", MachineType: "7z01", OsType: "none",     Scope: "latest", Status: "Completed", Timestamp: "2020-01-16T16:06:34Z" },
        OnecliAction{UUID: "bbd6dfd1-84a0-4df4-ba9d-cab3b930a4b2", Command: "update", Subcommand: "acquire", MachineType: "7z01", OsType: "platform", Scope: "latest", Status: "Completed", Timestamp: "2020-01-17T16:06:34Z" },
		OnecliAction{UUID: "be5fe8bf-bcc8-4dc7-855d-f9026376d7e7", Command: "update", Subcommand: "compare", User: "USERID", Password: "Meins12345", IP: "192.168.10.149", Status: "Completed", Timestamp: "2020-01-17T16:06:34Z" },
		OnecliAction{UUID: "f148d8ba-488b-4252-9ef1-200a0f2d5c92", Command: "update", Subcommand: "flash", User: "USERID", Password: "Meins12345", IP: "192.168.10.149", Updateid: "lnvgy_fw_lxpm_pdl130j-2.02_anyos_noarch", Status: "Completed", Timestamp: "2020-01-17T16:06:34Z" },
		OnecliAction{UUID: "ff8d234a-5c47-4a84-814a-45b5e2b55cf7", Command: "misc", Subcommand: "rebootbmc", User: "USERID", Password: "Meins12345", IP: "192.168.10.149", RebootAllowed: "No", Status: "Completed", Timestamp: "2020-01-17T16:06:34Z" },		
    }
		
	var updateOnecliAction OnecliAction
	
	if !oa.dbempty () {
		return
	}
	
	for _,updateOnecliAction = range InitOnecliActions {
		_, err := oa.addSingleOnecliAction(updateOnecliAction)
		if err != nil {
			log.Println (err.Error())
			break
		}
		_, err = oa.updateSingleOnecliAction(updateOnecliAction)
		if err != nil {
			log.Println (err.Error())
			break
		}
	}
	
	return
}

/*

HTTP status codes as registered with IANA:

see:	src/net/http/status.go
		
		https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
		
		StatusOK			= 200 // RFC 7231, 6.3.1
		StatusUnauthorized	= 401 // RFC 7235, 3.1

*/

//
// func publicKey returns the public key in the right type
//
func (oa *OnecliApi) publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	case ed25519.PrivateKey:
		return k.Public().(ed25519.PublicKey)
	default:
		return nil
	}
}

//
// func generateCertAndKey generates the certificates tls.key and tls.crt
//
func (oa *OnecliApi) generateCertAndKey () error {

	var notBefore time.Time
	var err		error
	var priv 	interface{}
	var certexists bool
	
	err = nil
	
	certexists = true
	oa.certfile = fmt.Sprintf ("%s/tls.crt",oa.oneclidir)
	if _, err := os.Stat(oa.certfile); os.IsNotExist(err) {
		certexists = false
	}
	oa.keyfile = fmt.Sprintf ("%s/tls.key",oa.oneclidir)
	if _, err := os.Stat(oa.keyfile); os.IsNotExist(err) {
		certexists = false
	}
	if certexists {
		log.Printf ("%s and %s already exist\n",oa.certfile,oa.keyfile)
		return err
	}
	
	if len(host) == 0 {
		err = fmt.Errorf("len host is 0")
		return err
	}

	switch ecdsaCurve {
	case "":
		if ed25519Key {
			_, priv, err = ed25519.GenerateKey(rand.Reader)
		} else {
			priv, err = rsa.GenerateKey(rand.Reader, rsaBits)
		}
	case "P224":
		priv, err = ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	case "P256":
		priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	case "P384":
		priv, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	case "P521":
		priv, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	default:
		err = fmt.Errorf("Unrecognized elliptic curve: %q", ecdsaCurve)
		return err
	}
	if err != nil {
		err = fmt.Errorf("Failed to generate private key: %v", err)
		return err
	}
	
	// ECDSA, ED25519 and RSA subject keys should have the DigitalSignature
	// KeyUsage bits set in the x509.Certificate template
	keyUsage := x509.KeyUsageDigitalSignature
	// Only RSA subject keys should have the KeyEncipherment KeyUsage bits set. In
	// the context of TLS this KeyUsage is particular to RSA key exchange and
	// authentication.
	if _, isRSA := priv.(*rsa.PrivateKey); isRSA {
		keyUsage |= x509.KeyUsageKeyEncipherment
	}

	notBefore = time.Now()
	notAfter := notBefore.Add(validFor)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		err = fmt.Errorf("Failed to generate serial number: %v", err)
		return err
	}

	hosts := strings.Split(host, ",")
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			//
			Organization:  []string{"Gardener"},
			Country:       []string{"DE"},
			Province:      []string{""},
			Locality:      []string{"Damrstadt"},
			StreetAddress: []string{"Heinrich-Hertz-Strasse"},
			PostalCode:    []string{"64295"},			
			//CommonName:    "localhost",
			CommonName:    hosts[0],
			//
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              keyUsage,
		//ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}

	if isCA {
		template.IsCA = true
		template.KeyUsage |= x509.KeyUsageCertSign
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, oa.publicKey(priv), priv)
	if err != nil {
		err = fmt.Errorf("Failed to create certificate: %v", err)
		return err
	}

	certOut, err := os.Create(oa.certfile)
	if err != nil {
		err = fmt.Errorf("Failed to open tls.crt for writing: %v", err)
		return err
	}
	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		err = fmt.Errorf("Failed to write data to tls.crt: %v", err)
		return err
	}
	if err := certOut.Close(); err != nil {
		err = fmt.Errorf("Error closing tls.crt: %v", err)
		return err
	}
	log.Printf("wrote %s\n",oa.certfile)

	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		err = fmt.Errorf("Unable to marshal private key: %v", err)
		return err
	}

	keyOut, err := os.OpenFile(oa.keyfile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		err = fmt.Errorf("Failed to open key file for writing: %v", err)
		return err
	}
	if err := pem.Encode(keyOut, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes}); err != nil {
		err = fmt.Errorf("Failed to write data to key file: %v", err)
		return err
	}
	if err := keyOut.Close(); err != nil {
		err = fmt.Errorf("Error closing key file: %v", err)
		return err
	}
	log.Printf("wrote %s\n",oa.keyfile)

	return err
}

//
// func getHash returns a sha256sum hash from a byte slice
//
func (oa *OnecliApi) getHash(text []byte) string {

    hasher := sha256.New()
    hasher.Write(text)
    return hex.EncodeToString(hasher.Sum(nil))
}

//
// func generateDefaultUserAndPassword generates an user account and a password
// from the certificate files
// user: sha256sum of file tls.crt
// password: sha256sum of file tls.key
//
func (oa *OnecliApi) generateDefaultUserAndPassword () error {

	var err error
	var cert []byte
	
	err = nil
	
	if len(oa.certfile) == 0 {
		err = fmt.Errorf ("error func generateDefaultUserAndPassword: len(oa.certfile) == 0")
		return err
	}

	if len(oa.keyfile) == 0 {
		err = fmt.Errorf ("error func generateDefaultUserAndPassword: len(oa.keyfile) == 0")
		return err
	}

	cert, err = ioutil.ReadFile(oa.certfile)
	if err != nil {
		return err
	}
	oa.username = oa.getHash (cert)

	cert, err = ioutil.ReadFile(oa.keyfile)
	if err != nil {
		return err
	}
	oa.password = oa.getHash (cert)
	
	return err
}

//
// func basicAuth checks user and password included in the header of the HTTP request
//
func (oa *OnecliApi) basicAuth (w http.ResponseWriter, r *http.Request) bool {

	u, p, ok := r.BasicAuth()
	if !ok {
		log.Println("Error parsing basic auth")
		w.WriteHeader(http.StatusUnauthorized)
		return ok
	}
	if u != oa.username {
		log.Printf("Username provided is not correct: %s\n", u)
		w.WriteHeader(http.StatusUnauthorized)
		return false
	}
	if p != oa.password {
		log.Printf("Password provided is not correct: %s\n", p)
		w.WriteHeader(http.StatusUnauthorized)
		return false
	}
	return ok
}

//
// see: https://golang.org/pkg/net/http/
//
// type ResponseWriter interface
//
// Write writes the data to the connection as part of an HTTP reply.
//
// If WriteHeader has not yet been called, Write calls
// WriteHeader(http.StatusOK) before writing the data. If the Header
// does not contain a Content-Type line, Write adds a Content-Type set
// to the result of passing the initial 512 bytes of written data to
// DetectContentType. Additionally, if the total size of all written
// data is under a few KB and there are no Flush calls, the
// Content-Length header is added automatically.
//

//
// func homePage creates a simple HTTP response
//
func (oa *OnecliApi) homePage(w http.ResponseWriter, r *http.Request){

    log.Println("Endpoint Hit: homePage")
	if !oa.basicAuth (w, r) {
		fmt.Fprintf(w, "Unauthorized !")
	} else  {
		fmt.Fprintf(w, "Welcome to the Onecli REST API server!")
	}
}

//
// func getAllOnecliActionsHandler returns all actions in JSON format
//
func (oa *OnecliApi) getAllOnecliActionsHandler(w http.ResponseWriter, r *http.Request){
    
	var sequences	 		[]uint64
	var sequence			uint64

	var allonecliactions 	[]OnecliAction
	var boltdbaction		BoltdbAction

	log.Println("Endpoint Hit: getAllOnecliActionsHandler")
	
	if !oa.basicAuth (w, r) {
		boltdbaction.Onecliaction.Status = "error"
		allonecliactions = append (allonecliactions,boltdbaction.Onecliaction)
		json.NewEncoder(w).Encode(allonecliactions)
		return
	}
	
	oa.Lock()
	defer oa.Unlock()

	maponecliactions := make(map[uint64]OnecliAction)
	
	for k := range maponecliactions {
		delete(maponecliactions, k)
	}
	sequences = nil	
	
	err := oa.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(oa.bucketName)

		b.ForEach(func(k, v []byte) error {
			json.Unmarshal(v, &boltdbaction)
			sequence = boltdbaction.Sequence
			maponecliactions[sequence]=boltdbaction.Onecliaction
			sequences = append (sequences,sequence)			
			return nil
		})
		return nil
	})	

	sortsequences := uint64arr(sequences)
	sort.Sort(sortsequences)

	for _, sequence = range sortsequences {
		allonecliactions = append (allonecliactions,maponecliactions[sequence])
	}

	if err != nil {
		log.Println (err.Error())
	}	
	
    json.NewEncoder(w).Encode(allonecliactions)
}

//
// func getStatusOnecliHandler returns the status of the RESTful web server in JSON format
//
func (oa *OnecliApi) getStatusOnecliHandler(w http.ResponseWriter, r *http.Request){

	var count_created int
	var count_pending int
	var count_running int
	var count_failed int
	var count_completed int
	var count_all int
	
	var oneclistatus OnecliStatus
	var boltdbaction BoltdbAction

	count_created=0
	count_pending=0
	count_running=0
	count_failed=0
	count_completed=0
	count_all=0

    log.Println("Endpoint Hit: getStatusOnecliHandler")
	
	if !oa.basicAuth (w, r) {
		oneclistatus.Status = "error"
		json.NewEncoder(w).Encode(oneclistatus)
		return
	}	

	oa.Lock()
	defer oa.Unlock()
	
	err := oa.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(oa.bucketName)

		b.ForEach(func(k, v []byte) error {
			json.Unmarshal(v, &boltdbaction)
			switch boltdbaction.Onecliaction.Status {
				case "Created":
					count_created++
				case "Pending":
					count_pending++
				case "Running":
					count_running++
				case "Failed":
					count_failed++
				case "Completed":
					count_completed++
				default:
					count_pending++
			}
			count_all++
			return nil
		})
		return nil
	})	

	if err != nil {
		log.Println("getStatusOnecliHandler: db.View failed")
	}	
	
	if count_all == (count_created + count_pending + count_running + count_failed + count_completed) {
		oneclistatus.Status = "ok"
	} else {
		oneclistatus.Status = "error"
	}
	oneclistatus.Created = fmt.Sprintf ("%d",count_created)
	oneclistatus.Pending = fmt.Sprintf ("%d",count_pending)
	oneclistatus.Running = fmt.Sprintf ("%d",count_running)
	oneclistatus.Failed = fmt.Sprintf ("%d",count_failed)
	oneclistatus.Completed = fmt.Sprintf ("%d",count_completed)
	oneclistatus.All = fmt.Sprintf ("%d",count_all)
    
	json.NewEncoder(w).Encode(oneclistatus)	
}

//
// func getOnecliActionHandler returns a single action in JSON format
//
func (oa *OnecliApi) getOnecliActionHandler(w http.ResponseWriter, r *http.Request){

	var boltdbaction BoltdbAction

    vars := mux.Vars(r)
    key := vars["uuid"]

    log.Println("Endpoint Hit: getOnecliActionHandler")
	
	if !oa.basicAuth (w, r) {
		boltdbaction.Onecliaction.Status = "error"
		json.NewEncoder(w).Encode(boltdbaction.Onecliaction) // empty
		return
	}		

	oa.Lock()
	defer oa.Unlock()
	
	err := oa.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(oa.bucketName)
		v := b.Get([]byte(key))
		json.Unmarshal(v, &boltdbaction)
		return nil
	})	
	
	if err != nil {
		log.Println (err.Error())
	}	
	json.NewEncoder(w).Encode(boltdbaction.Onecliaction)
}

//
// func getOnecliActionsIpHandler returns all actions related to a dedicated IP in JSON format
//
func (oa *OnecliApi) getOnecliActionsIpHandler(w http.ResponseWriter, r *http.Request){

	var allonecliactions []OnecliAction
	var boltdbaction BoltdbAction

    vars := mux.Vars(r)
    key := vars["ip"]

    log.Println("Endpoint Hit: getOnecliActionsIpHandler")

	if !oa.basicAuth (w, r) {
		boltdbaction.Onecliaction.Status = "error"
		allonecliactions = append (allonecliactions,boltdbaction.Onecliaction)
		json.NewEncoder(w).Encode(allonecliactions)
		return
	}

	oa.Lock()
	defer oa.Unlock()
	
	err := oa.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(oa.bucketName)

		b.ForEach(func(k, v []byte) error {
			json.Unmarshal(v, &boltdbaction)
			if boltdbaction.Onecliaction.IP == key && boltdbaction.Onecliaction.IP != "" {
				allonecliactions = append (allonecliactions,boltdbaction.Onecliaction)
			}
			return nil
		})
		return nil
	})	
	
	if err != nil {
		log.Println (err.Error())
	}	
	
    json.NewEncoder(w).Encode(allonecliactions)	
}

//
// func getOnecliUpdateAcquireHandler returns all actions with
// Command = "update" and Subdcomand = "acquire"
//
func (oa *OnecliApi) getOnecliUpdateAcquireHandler(w http.ResponseWriter, r *http.Request){

	var allonecliactions []OnecliAction
	var boltdbaction BoltdbAction

    log.Println("Endpoint Hit: getOnecliUpdateAcquireHandler")
	
	if !oa.basicAuth (w, r) {
		boltdbaction.Onecliaction.Status = "error"
		allonecliactions = append (allonecliactions,boltdbaction.Onecliaction)
		json.NewEncoder(w).Encode(allonecliactions)
		return
	}	

	oa.Lock()
	defer oa.Unlock()
	
	err := oa.db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(oa.bucketName)

		b.ForEach(func(k, v []byte) error {
			json.Unmarshal(v, &boltdbaction)
			if boltdbaction.Onecliaction.Command == "update" && boltdbaction.Onecliaction.Subcommand == "acquire" {
				allonecliactions = append (allonecliactions,boltdbaction.Onecliaction)
			}
			return nil
		})
		return nil
	})	
	
	if err != nil {
		log.Println (err.Error())
	}	
	
    json.NewEncoder(w).Encode(allonecliactions)
}

//
// func addOnecliActionHandler adds a new action to the key-value store
// input: HTTP request in JSON fromat
//
func (oa *OnecliApi) addOnecliActionHandler(w http.ResponseWriter, r *http.Request) {

	var updateOnecliAction OnecliAction
	
    log.Println("Endpoint Hit: addOnecliActionHandler")
	
	if !oa.basicAuth (w, r) {
		updateOnecliAction.Status = "error"
		json.NewEncoder(w).Encode(updateOnecliAction)	
		return
	}
	
	oa.Lock()
	defer oa.Unlock()
	
    // get the body of our POST request
    // unmarshal this into a new OnecliAction struct
    // append this to our Articles array.    
    reqBody, _ := ioutil.ReadAll(r.Body)
	
    json.Unmarshal(reqBody, &updateOnecliAction)
	
	updateOnecliAction, err := oa.addSingleOnecliAction(updateOnecliAction)
	if err != nil {
		log.Println (err.Error())
		updateOnecliAction.Status = "Failed"
	}
	
    json.NewEncoder(w).Encode(updateOnecliAction)	
}

//
// func addSingleOnecliAction  adds a new action to the key-value store
// input: struct
//
func (oa *OnecliApi) addSingleOnecliAction(updateOnecliAction OnecliAction) (OnecliAction, error) {

	var boltdbaction BoltdbAction
	
	var err error

	err = nil
	
	boltdbaction.Onecliaction = updateOnecliAction

	if boltdbaction.Onecliaction.UUID == "" {
		// create new uuid
		uuid := guuid.New()
		boltdbaction.Onecliaction.UUID = uuid.String()
	}
	
	boltdbaction.Onecliaction.Status = "Created"
	timestamp := time.Now()
	boltdbaction.Onecliaction.Timestamp = timestamp.Format(time.RFC3339)
	
	err = oa.db.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists(oa.bucketName)
		if err != nil {
			return err
		}
		
		boltdbaction.Sequence, _ = bkt.NextSequence()

		key := []byte(boltdbaction.Onecliaction.UUID)
        value, err := json.Marshal(boltdbaction)
        if err != nil {
            return err
        }		
		
		err = bkt.Put(key, value)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return boltdbaction.Onecliaction, err
	}
	return boltdbaction.Onecliaction, err
}

//
// func deleteOnecliActionHandler deletes an action from the key-value store (boltdb)
//
func (oa *OnecliApi) deleteOnecliActionHandler(w http.ResponseWriter, r *http.Request) {

	var err 			error
    var onecliaction	OnecliAction	

    log.Println("Endpoint Hit: deleteOnecliActionHandler")
	
	if !oa.basicAuth (w, r) {
		onecliaction.Status = "error"
		json.NewEncoder(w).Encode(onecliaction)	
		return
	}	

	oa.Lock()
	defer oa.Unlock()
	
    // once again, we will need to parse the path parameters
    vars := mux.Vars(r)
	
    // we will need to extract the `uuid` of the action we
    // wish to delete
    key := vars["uuid"]
	
	onecliaction.UUID = key
	onecliaction.Status = "Deleted"
	timestamp := time.Now()
	onecliaction.Timestamp = timestamp.Format(time.RFC3339)
	
	dirname := fmt.Sprintf ("%s/%s",oa.logsdir,key)
	if _, err = os.Stat(dirname); os.IsNotExist(err) {
		log.Println("Error in func deleteOnecliActionHandler: Directory does not exist.")
	} else {		
		err = os.RemoveAll(dirname) 
		if err != nil { 
			log.Println (err.Error())
		}
	}
	
	err = oa.db.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket(oa.bucketName)
		err := b.Delete([]byte(key))

		return err
	})			
	if err != nil {
		log.Println (err.Error())
		onecliaction.Status = "Failed"
	}

	json.NewEncoder(w).Encode(onecliaction)	
}

//
// func updateSingleOnecliAction updates an action in the key-value store (boltdb)
// input: struct
//
func (oa *OnecliApi) updateSingleOnecliAction(updateOnecliAction OnecliAction) (OnecliAction, error) {

	var boltdbaction BoltdbAction
	var err error
	
	err = nil

	err = oa.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(oa.bucketName)
		v := b.Get([]byte(updateOnecliAction.UUID))
		json.Unmarshal(v, &boltdbaction)
		return nil
	})	
	
	if err != nil {
		return updateOnecliAction, err
	}	
	//
	// update content
	//
	if updateOnecliAction.Command != "" {
		boltdbaction.Onecliaction.Command = updateOnecliAction.Command
	}
	if updateOnecliAction.Subcommand != "" {
		boltdbaction.Onecliaction.Subcommand = updateOnecliAction.Subcommand
	}
	if updateOnecliAction.MachineType != "" {
		boltdbaction.Onecliaction.MachineType = updateOnecliAction.MachineType
	}
	if updateOnecliAction.OsType != "" {
		boltdbaction.Onecliaction.OsType = updateOnecliAction.OsType
	}
	if updateOnecliAction.Scope != "" {
		boltdbaction.Onecliaction.Scope = updateOnecliAction.Scope
	}
	if updateOnecliAction.User != "" {
		boltdbaction.Onecliaction.User = updateOnecliAction.User
	}
	if updateOnecliAction.Password != "" {
		boltdbaction.Onecliaction.Password = updateOnecliAction.Password
	}
	if updateOnecliAction.IP != "" {
		boltdbaction.Onecliaction.IP = updateOnecliAction.IP
	}
	if updateOnecliAction.Status != "" {
		boltdbaction.Onecliaction.Status = updateOnecliAction.Status
	}
	if updateOnecliAction.Timestamp != "" {
		boltdbaction.Onecliaction.Timestamp = updateOnecliAction.Timestamp
	}
	//
	// write
	//
	err = oa.db.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists(oa.bucketName)
		if err != nil {
			return err
		}
		
        value, err := json.Marshal(boltdbaction)
        if err != nil {
            return err
        }		
		
		err = bkt.Put([]byte(updateOnecliAction.UUID), value)
		if err != nil {
			return err
		}
		return nil
	})
	
	if err != nil {
		return boltdbaction.Onecliaction, err
	}
	return boltdbaction.Onecliaction, err
}

//
// func updateOnecliActionHandler updates an action in the key-value store (boltdb)
// input: HTTP request
//
func (oa *OnecliApi) updateOnecliActionHandler(w http.ResponseWriter, r *http.Request) {

	var updateOnecliAction OnecliAction
	
	var err error
	
	err = nil
	
	//
	// read
	//
    vars := mux.Vars(r)
    key := vars["uuid"]

    log.Println("Endpoint Hit: updateOnecliActionHandler")
	
	if !oa.basicAuth (w, r) {
		updateOnecliAction.Status = "error"
		json.NewEncoder(w).Encode(updateOnecliAction)	
		return
	}
	
	oa.Lock()
	defer oa.Unlock()
	
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Kindly enter data in order to update")
	}
	json.Unmarshal(reqBody, &updateOnecliAction)

	updateOnecliAction.UUID = key	
	
	updateOnecliAction, err = oa.updateSingleOnecliAction(updateOnecliAction)
	
	if err != nil {
		log.Println (err.Error())
		updateOnecliAction.Status = "Failed"
	}
	
    json.NewEncoder(w).Encode(updateOnecliAction)	
	
}

//
// func executeOnecli executes the actions in an endless loop
// runs as a background process
//
func (oa *OnecliApi) executeOnecli() {

	//var maponecliactions	map[uint64]OnecliAction
	var sequences	 		[]uint64
	//var sortsequences 		sort.Interface
	var sequence			uint64
	var boltdbaction 		BoltdbAction
	var onecliaction		OnecliAction
	var updateOnecliAction	OnecliAction
	var completelogsdirpath string
	//var err 				error
	
	maponecliactions := make(map[uint64]OnecliAction)
	
	log.Println("Start: executeOnecli")

	defer oa.closedb()

    for {

		updateOnecliAction.UUID				= ""
		updateOnecliAction.Command			= ""
		updateOnecliAction.Subcommand		= ""
		updateOnecliAction.MachineType		= ""
		updateOnecliAction.OsType			= ""
		updateOnecliAction.Scope			= ""
		updateOnecliAction.User				= ""
		updateOnecliAction.Password			= ""
		updateOnecliAction.IP				= ""
		updateOnecliAction.Updateid			= ""
		updateOnecliAction.RebootAllowed	= ""
		updateOnecliAction.Status			= ""
		updateOnecliAction.Timestamp		= ""
		
		for k := range maponecliactions {
			delete(maponecliactions, k)
		}
		sequences = nil
		//sortsequences = nil
		
		oa.Lock()
		err := oa.db.View(func(tx *bolt.Tx) error {
			// Assume bucket exists and has keys
			b := tx.Bucket(oa.bucketName)

			b.ForEach(func(k, v []byte) error {
				json.Unmarshal(v, &boltdbaction)
				sequence = boltdbaction.Sequence
				maponecliactions[sequence]=boltdbaction.Onecliaction
				sequences = append (sequences,sequence)
				return nil
			})
			return nil
		})	
		oa.Unlock()
		
		sortsequences := uint64arr(sequences)
		sort.Sort(sortsequences)
	
		for _, sequence = range sortsequences {
			onecliaction = maponecliactions[sequence]
			switch onecliaction.Status {
				case "Created":
					updateOnecliAction.UUID = onecliaction.UUID
					updateOnecliAction.Status = "Running"
					timestamp := time.Now()
					updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
					oa.Lock()
					oa.updateSingleOnecliAction(updateOnecliAction)			
					oa.Unlock()
			
					command := oa.createCommand(onecliaction)
					log.Println("execute command: '"+command+"'")
				
					if command != "" {
						err = oa.checkConnection (onecliaction)
						if err != nil {
							log.Println("Error in func executeOnecli: checkConnection failed")
							updateOnecliAction.UUID = onecliaction.UUID
							updateOnecliAction.Status = "Failed"
							timestamp := time.Now()
							updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
							oa.Lock()
							oa.updateSingleOnecliAction(updateOnecliAction)			
							oa.Unlock()
						} else {
							completelogsdirpath = oa.logsdir + "/" + updateOnecliAction.UUID
							err = oa.makeDirectoryIfNotExists(completelogsdirpath)
							if err != nil {
								log.Println("Error in func executeOnecli: Could not create logs directory")
								updateOnecliAction.UUID = onecliaction.UUID
								updateOnecliAction.Status = "Failed"
								timestamp := time.Now()
								updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
								oa.Lock()
								oa.updateSingleOnecliAction(updateOnecliAction)			
								oa.Unlock()
							} else {
								err = oa.runCommand (command)
								if err != nil {
									log.Println("Error in func executeOnecli: runCommand failed")
									updateOnecliAction.UUID = onecliaction.UUID
									updateOnecliAction.Status = "Failed"
									timestamp := time.Now()
									updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
									oa.Lock()
									oa.updateSingleOnecliAction(updateOnecliAction)			
									oa.Unlock()
								} else {
									updateOnecliAction.UUID = onecliaction.UUID
									updateOnecliAction.Status = "Completed"
									timestamp := time.Now()
									updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
									oa.Lock()
									oa.updateSingleOnecliAction(updateOnecliAction)			
									oa.Unlock()
								}
								if oa.simulator {
									_, err = oa.copyresultfile ("/simulator/common_result.xml",completelogsdirpath + "/common_result.xml")
									if err != nil {
										log.Println("Error in func executeOnecli: copy simulator files failed")
										updateOnecliAction.UUID = onecliaction.UUID
										updateOnecliAction.Status = "Failed"
										timestamp := time.Now()
										updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
										oa.Lock()
										oa.updateSingleOnecliAction(updateOnecliAction)			
										oa.Unlock()
									}
									_, err = oa.copyresultfile ("/simulator/Onecli-update-compare.xml",completelogsdirpath + "/Onecli-update-compare.xml")
									if err != nil {
										log.Println("Error in func executeOnecli: copy simulator files failed")
										updateOnecliAction.UUID = onecliaction.UUID
										updateOnecliAction.Status = "Failed"
										timestamp := time.Now()
										updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
										oa.Lock()
										oa.updateSingleOnecliAction(updateOnecliAction)			
										oa.Unlock()
									}
									_, err = oa.copyresultfile ("/simulator/Onecli-update-query.xml",completelogsdirpath + "/Onecli-update-query.xml")
									if err != nil {
										log.Println("Error in func executeOnecli: copy simulator files failed")
										updateOnecliAction.UUID = onecliaction.UUID
										updateOnecliAction.Status = "Failed"
										timestamp := time.Now()
										updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
										oa.Lock()
										oa.updateSingleOnecliAction(updateOnecliAction)			
										oa.Unlock()
									}
									_, err = oa.copyresultfile ("/simulator/Onecli-update-scan.xml",completelogsdirpath + "/Onecli-update-scan.xml")
									if err != nil {
										log.Println("Error in func executeOnecli: copy simulator files failed")
										updateOnecliAction.UUID = onecliaction.UUID
										updateOnecliAction.Status = "Failed"
										timestamp := time.Now()
										updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
										oa.Lock()
										oa.updateSingleOnecliAction(updateOnecliAction)			
										oa.Unlock()
									}
								}
							}
						}
					} else {
						log.Println("Error in func executeOnecli: commend empty")
						updateOnecliAction.UUID = onecliaction.UUID
						updateOnecliAction.Status = "Failed"
						timestamp := time.Now()
						updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
						oa.Lock()
						oa.updateSingleOnecliAction(updateOnecliAction)			
						oa.Unlock()
					}
					time.Sleep(1000 * time.Millisecond)
					break // will break the loop to start from beginning of the action list
				case "Running":
					log.Println("Error in func executeOnecli: Status 'Runing' found, set Satus to 'Failed'")
					updateOnecliAction.UUID = onecliaction.UUID
					updateOnecliAction.Status = "Failed"
					timestamp := time.Now()
					updateOnecliAction.Timestamp = timestamp.Format(time.RFC3339)
					oa.Lock()
					oa.updateSingleOnecliAction(updateOnecliAction)			
					oa.Unlock()
					time.Sleep(1000 * time.Millisecond)
					break
				default:	
					time.Sleep(50 * time.Millisecond)
			}
		}			
        time.Sleep(50 * time.Millisecond)
    }
	os.Exit(1) // we should never reach this
}

//
// func makeDirectoryIfNotExists creates a directory
//
func (oa *OnecliApi) makeDirectoryIfNotExists(path string) error {
	log.Println ("Create directory '"+path+"'")
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return os.Mkdir(path, os.ModeDir|0755)
	}
	return nil
}

//
// func createCommand creates a command line using action's content 
//
func (oa *OnecliApi) createCommand(executeOnecliAction OnecliAction) string {
/*

	download:
    ./onecli update acquire --mt $bmctype --ostype none --scope latest --dir /pool
    ./onecli update acquire --mt $bmctype --ostype platform --scope latest --dir /pool
	
	scan:
	./onecli update compare --bmc USERID:<pass>@<ip of ipmi> --dir pool
	./onecli update compare --bmc USERID:Meins12345@192.168.10.149 --dir /srv/firmware/lenovo/pool
	./onecli update compare --bmc USERID:Meins12345@192.168.10.149 --dir /srv/firmware/lenovo/pool --output ./logs/llauber
	
	flash:
	./onecli update flash --bmc USERID:<pass>@<ip of ipmi> --dir pool --includeid lnvgy_fw_lxpm_pdl130j-2.02_anyos_noarch <updateID
	./onecli update flash --bmc USERID:<pass>@<ip of ipmi> --dir pool --nocompare --includeid lnvgy_fw_lxpm_pdl130j-2.02_anyos_noarch <updateID
	
	reboot:
	OneCli.exe misc rebootbmc --bmc userid:password@host[:port]
	
	syshealth:
	OneCli.exe misc syshealth --device system --bmc userid:password@host

*/
	var command string
	var completelogsdirpath string
	
	command = ""
	completelogsdirpath = oa.logsdir + "/" + executeOnecliAction.UUID

	switch executeOnecliAction.Command {
		case "update":
			switch executeOnecliAction.Subcommand {
				case "acquire":
					if executeOnecliAction.MachineType != "" && executeOnecliAction.OsType != "" && executeOnecliAction.Scope != "" {
						command = fmt.Sprintf ("%s/onecli update acquire --mt %s --ostype %s --scope %s --dir %s --output %s -q",
												oa.oneclidir,
												executeOnecliAction.MachineType,
												executeOnecliAction.OsType,
												executeOnecliAction.Scope,
												oa.pooldir,
												completelogsdirpath)
					} else {
						log.Println("Error in func createCommand: Missing parameter mt or ostype or scope")
					}
				case "compare":
					if executeOnecliAction.User != "" && executeOnecliAction.Password != "" && executeOnecliAction.IP != "" {
						command = fmt.Sprintf ("%s/onecli update compare --bmc %s:%s@%s --dir %s --output %s -q",
												oa.oneclidir,
												executeOnecliAction.User,
												executeOnecliAction.Password,
												executeOnecliAction.IP,
												oa.pooldir,
												completelogsdirpath)
					} else {
						log.Println("Error in func createCommand: Missing parameter mt or ostype or scope")
					}	
				case "flash":
					if executeOnecliAction.User != "" && executeOnecliAction.Password != "" && executeOnecliAction.IP != "" && executeOnecliAction.Updateid != ""{
						//command = fmt.Sprintf ("%s/onecli update flash --bmc %s:%s@%s  --includeid %s --nocompare --noreboot --dir %s --output %s -q",
						command = fmt.Sprintf ("%s/onecli update flash --bmc %s:%s@%s  --includeid %s --nocompare --dir %s --output %s -q",
												oa.oneclidir,
												executeOnecliAction.User,
												executeOnecliAction.Password,
												executeOnecliAction.IP,
												executeOnecliAction.Updateid,
												oa.pooldir,
												completelogsdirpath)
					} else {
						log.Println("Error in func createCommand: Missing parameter user or password or IP or updateid.")
					}					
				default:
					log.Println("Error in func createCommand: SubCommand not found")
			}
		case "misc":
			switch executeOnecliAction.Subcommand {
				case "rebootbmc":
					if executeOnecliAction.User != "" && executeOnecliAction.Password != "" && executeOnecliAction.IP != "" && executeOnecliAction.RebootAllowed != ""{
						command = fmt.Sprintf ("%s/onecli misc rebootbmc --bmc %s:%s@%s --output %s -q",
												oa.oneclidir,
												executeOnecliAction.User,
												executeOnecliAction.Password,
												executeOnecliAction.IP,
												completelogsdirpath)
					} else {
						log.Println("Error in func createCommand: Missing parameter user or password or IP or RebootAllowed.")
					}					
				default:
					log.Println("Error in func createCommand: SubCommand not found")
			}
		
		default:
			log.Println("Error in func createCommand: Command not found")
	}
	
	if oa.simulator {
		command = "echo " + command
	}
	
	return command
}

//
// func checkConnectionHostPort checks, if a network connection to the Lenovo target system is available
//
func (oa *OnecliApi) checkConnectionHostPort(host string, port string) error {

	var err error
	
	err = nil
	
	if oa.simulator  {
		return err
	}
	
	if host == "" || port == "" {
		err = errors.New("Error in func checkConnection: IP or port emtpty.")
		return err
	}	

	timeout := time.Duration(1 * time.Second)
	_, err = net.DialTimeout("tcp", host+":"+port, timeout)
	if err != nil {
		log.Printf("%s %s %s\n", host, "not responding", err.Error())
		return err
	} else {
		log.Printf("%s %s %s\n", host, "responding on port:", port)
	}
	return err
}

//
// func checkConnection executes the func checkConnectionHostPort
// depending on the command and subcommand  of the action
//
func (oa *OnecliApi) checkConnection(executeOnecliAction OnecliAction) error {

	var err error
	var port string

	err = nil
	port = "115"

	switch executeOnecliAction.Command {
			case "update":
				switch executeOnecliAction.Subcommand {
					case "acquire":
						err = nil
						return err
					case "compare":
						err = oa.checkConnectionHostPort(executeOnecliAction.IP, port)
						return err
					case "flash":
						err = oa.checkConnectionHostPort(executeOnecliAction.IP, port)
						return err
					default:
						log.Println("Error in func checkConnection: SubCommand not found")
						err = errors.New("Error in func checkConnection: Subcommand not found.")
						return err
				}
			case "misc":
				switch executeOnecliAction.Subcommand {
					case "rebootbmc":
						err = oa.checkConnectionHostPort(executeOnecliAction.IP, port)
						return err
					default:
						log.Println("Error in func checkConnection: SubCommand not found")
						err = errors.New("Error in func checkConnection: Subcommand not found.")
						return err
				}
			default:
				log.Println("Error in func checkConnection: Command not found")
				err = errors.New("Error in func checkConnection: Command not found.")
				return err
	}
	return err
}

//
// func runCommand executes the comand line generated by func createCommand
//
func (oa *OnecliApi) runCommand(command string) error {

	// See https://regexr.com/4154h for custom regex to parse commands
	// Inspired by https://gist.github.com/danesparza/a651ac923d6313b9d1b7563c9245743b
	pattern := `(--[^\s]+="[^"]+")|"([^"]+)"|'([^']+)'|([^\s]+)`
	parts := regexp.MustCompile(pattern).FindAllString(command, -1)

	//	The first part is the command, the rest are the args:
	head := parts[0]
	arguments := parts[1:len(parts)]

	//	Format the command
	cmd := exec.Command(head, arguments...)

    cmd.Stdin = os.Stdin
    cmd.Stderr = os.Stderr
    stdOut, err := cmd.StdoutPipe()
    if err != nil {
        log.Println("Error in func runCommand: cmd.StdoutPipe()")
		return err
    }
    if err := cmd.Start(); err != nil {
        log.Println("Error in func runCommand: cmd.Start()")
		return err
    }
    bytes, err := ioutil.ReadAll(stdOut)
    if err != nil {
        log.Println("Error in func runCommand: ioutil.ReadAll(stdOut)")
		return err
    }
    if err := cmd.Wait(); err != nil {
        log.Println("Error in func runCommand: cmd.Wait()")
        if exitError, ok := err.(*exec.ExitError); ok {
            log.Printf("Exit code is %d\n", exitError.ExitCode())
        }
		// this would be very strange
		if err == nil {
			err = errors.New("Error in func runCommand: exit code != 0 but err == nil")
		}
		
		log.Println(string(bytes))
		return err
    }
    log.Println(string(bytes))
	return err
}

//
// func copyresultfile copies a file
//
func (oa *OnecliApi) copyresultfile (src string, dst string) (int64, error) {

		log.Printf ("***** Filecopy *****")
        log.Printf ("Source: %s\n",src)
		log.Printf ("Dest. : %s\n",dst)
		
		sourceFileStat, err := os.Stat(src)
        if err != nil {
				log.Printf ("func copyresultfile, os.Stat error: %s\n",err.Error())
                return 0, err
        }

        if !sourceFileStat.Mode().IsRegular() {
				log.Printf ("func copyresultfile, IsRegular error: %s is not a regular file", src)
                return 0, fmt.Errorf("%s is not a regular file", src)
        }

        source, err := os.Open(src)
        if err != nil {
				log.Printf ("func copyresultfile, os.Open error: %s\n",err.Error())
                return 0, err
        }
        defer source.Close()

        destination, err := os.Create(dst)
        if err != nil {
				log.Printf ("func copyresultfile, os.Create error: %s\n",err.Error())
                return 0, err
        }
        defer destination.Close()
        nBytes, err := io.Copy(destination, source)
        if err != nil {
				log.Printf ("func copyresultfile, io.Copy error: %s\n",err.Error())
                return 0, err
        }
        return nBytes, err
}

//
// func getOnecliXmlcommonresultHandler return the content of the file common_result.xml
// of an action in JSON format
//
func (oa *OnecliApi) getOnecliXmlcommonresultHandler(w http.ResponseWriter, r *http.Request){

	var filename string
    var cr CommonResult
    var crjson CommonResultJson
	var modulejson ModuleJson
	var boltdbaction BoltdbAction

    vars := mux.Vars(r)
    key := vars["uuid"]

    log.Println("Endpoint Hit: getOnecliXmlcommonresultHandler")
	
	if !oa.basicAuth (w, r) {
		crjson.Error = "Yes"
		crjson.Message = "Unauthorized"

		modulejson.Name = "Unknown"
		modulejson.Returncode = "1"
		modulejson.Errormessage = "Unauthorized."
	    crjson.Modules.Modules = append(crjson.Modules.Modules, modulejson)		
		
		json.NewEncoder(w).Encode(crjson)
		return
	}
	
	oa.Lock()
	defer oa.Unlock()
	
	filename = ""

	err := oa.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(oa.bucketName)
		v := b.Get([]byte(key))
		json.Unmarshal(v, &boltdbaction)
		return nil
	})	
	
	if err != nil {
		log.Println (err.Error())
		crjson.Error = "Yes"
		crjson.Message = "UUID not found or action not in status 'Completed'."
		crjson.Onecli = "Unknown"
		crjson.Logfile = filename

		modulejson.Name = "Unknown"
		modulejson.Returncode = "1"
		modulejson.Errormessage = "Failed."
	    crjson.Modules.Modules = append(crjson.Modules.Modules, modulejson)	

		boltdbaction.Onecliaction.Status = "Failed"
	}
	if boltdbaction.Onecliaction.UUID == key && boltdbaction.Onecliaction.Status == "Completed" {
		filename = fmt.Sprintf ("%s/%s/common_result.xml",oa.logsdir,boltdbaction.Onecliaction.UUID)
		log.Println ("Log-file-name: "+filename)

		// Open our xmlFile
		xmlFile, err := os.Open(filename)
		// if we os.Open returns an error then handle it
		if err != nil {
			crjson.Error = "Yes"
			crjson.Message = "Could not open file common_result.xml."
			crjson.Onecli = "Unknown"
			crjson.Logfile = filename

			modulejson.Name = "Unknown"
			modulejson.Returncode = "1"
			modulejson.Errormessage = "Failed."
			crjson.Modules.Modules = append(crjson.Modules.Modules, modulejson)		
		} else {
		
			// defer the closing of our xmlFile so that we can parse it later on
			defer xmlFile.Close()

			// read our opened xmlFile as a byte array.
			byteValue, _ := ioutil.ReadAll(xmlFile)

			// we initialize our CommonResult array
			// we unmarshal our byteArray which contains our
			// xmlFiles content into 'cr' which we defined above
			xml.Unmarshal(byteValue, &cr)

			crjson.Error = cr.Error
			crjson.Message = cr.Message
			crjson.Onecli = cr.Onecli
			crjson.Logfile = cr.Logfile

			// we iterate through every CommonResult.Module within our users array and
			// print out the user Type, their name, and their facebook url
			// as just an example
			for i := 0; i < len(cr.Modules.Modules); i++ {
				modulejson.Name = cr.Modules.Modules[i].Name
				modulejson.Returncode = cr.Modules.Modules[i].Returncode
				modulejson.Errormessage = cr.Modules.Modules[i].Errormessage
				crjson.Modules.Modules = append(crjson.Modules.Modules, modulejson)		
			}
		}
	} else {
		crjson.Error = "Yes"
		crjson.Message = "UUID not found or action not in status 'Completed'."
		crjson.Onecli = "Unknown"
		crjson.Logfile = filename

		modulejson.Name = "Unknown"
		modulejson.Returncode = "1"
		modulejson.Errormessage = "Failed."
	    crjson.Modules.Modules = append(crjson.Modules.Modules, modulejson)		
	}
	json.NewEncoder(w).Encode(crjson)
}

//
// func getOnecliXmlupdatecompareHandler return the content of the file Onecli-update-compare.xml
// of an action in JSON format
//
func (oa *OnecliApi) getOnecliXmlupdatecompareHandler(w http.ResponseWriter, r *http.Request){

	var filename string
	var boltdbaction BoltdbAction

    var comparison Comparison
	var childupdatejson ChildupdateJson
	var packagejson PackageJson
	var contentjson ContentJson
	var comparisonjson ComparisonJson
	var rootjson RootJson

    vars := mux.Vars(r)
    key := vars["uuid"]

    log.Println("Endpoint Hit: getOnecliXmlcommonresultHandler")
	
	if !oa.basicAuth (w, r) {
		rootjson.Error = "Yes"
		rootjson.Message = "Unauthorized"
		json.NewEncoder(w).Encode(rootjson)
		return
	}
	
	oa.Lock()
	defer oa.Unlock()
	
	filename = ""
	
	err := oa.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(oa.bucketName)
		v := b.Get([]byte(key))
		json.Unmarshal(v, &boltdbaction)
		return nil
	})		
	if err != nil {
		log.Println (err.Error())

		rootjson.Error = "Yes"
		rootjson.Message = "Could not open file common_result.xml."
		rootjson.Logfile = filename

		boltdbaction.Onecliaction.Status = "Failed"
	}
	
	if boltdbaction.Onecliaction.UUID == key && boltdbaction.Onecliaction.Status == "Completed" {
		filename = fmt.Sprintf ("%s/%s/Onecli-update-compare.xml",oa.logsdir,boltdbaction.Onecliaction.UUID)
		log.Println ("Log-file-name: "+filename)

		// Open our xmlFile
		xmlFile, err := os.Open(filename)
		// if we os.Open returns an error then handle it
		if err != nil {
			rootjson.Error = "Yes"
			rootjson.Message = "Could not open file common_result.xml."
			rootjson.Logfile = filename
		} else {
		
			// defer the closing of our xmlFile so that we can parse it later on
			defer xmlFile.Close()
			
			// read our opened xmlFile as a byte array.
		    byteValue, _ := ioutil.ReadAll(xmlFile)

    		// we initialize our Comparison array
			// we unmarshal our byteArray which contains our
			// xmlFiles content into 'comp' which we defined above
			xml.Unmarshal(byteValue, &comparison)

			rootjson.Error = "No"
			rootjson.Message = "File common_result.xml read successfully."
			rootjson.Logfile = filename

			contentjson.Mt = comparison.Content.Mt
			contentjson.Bmctype = comparison.Content.Bmctype
			contentjson.Os = comparison.Content.Os
			contentjson.Arch = comparison.Content.Arch
			contentjson.Scope = comparison.Content.Scope
			contentjson.Type = comparison.Content.Type
			contentjson.Directory = comparison.Content.Directory
			contentjson.Ispartition = comparison.Content.Ispartition
			contentjson.Total = comparison.Content.Total
		
			contentjson.Packages = nil
	
			for i := 0; i < len(comparison.Content.Packages.Packages); i++ {
				packagejson.Name = comparison.Content.Packages.Packages[i].Name
				packagejson.Component = comparison.Content.Packages.Packages[i].Component
				packagejson.Severity = comparison.Content.Packages.Packages[i].Severity
				packagejson.Severitydes = comparison.Content.Packages.Packages[i].Severitydes
				packagejson.Reboot = comparison.Content.Packages.Packages[i].Reboot
				packagejson.Rebootdes = comparison.Content.Packages.Packages[i].Rebootdes
				packagejson.Updateid = comparison.Content.Packages.Packages[i].Updateid
				packagejson.Pkgversion = comparison.Content.Packages.Packages[i].Pkgversion
				packagejson.Requisite = comparison.Content.Packages.Packages[i].Requisite
				packagejson.Prerequisite = comparison.Content.Packages.Packages[i].Prerequisite
				packagejson.Corequisite = comparison.Content.Packages.Packages[i].Corequisite
				packagejson.Packagexml = comparison.Content.Packages.Packages[i].Packagexml
				packagejson.Uxspxml = comparison.Content.Packages.Packages[i].Uxspxml
				packagejson.Payloadfile = comparison.Content.Packages.Packages[i].Payloadfile
				packagejson.Commandline = comparison.Content.Packages.Packages[i].Commandline
				packagejson.Cinstalled = comparison.Content.Packages.Packages[i].Cinstalled
				packagejson.Alsoselect = comparison.Content.Packages.Packages[i].Alsoselect
				packagejson.Alsodesselect = comparison.Content.Packages.Packages[i].Alsodesselect
				packagejson.Selected = comparison.Content.Packages.Packages[i].Selected
				packagejson.Forceid = comparison.Content.Packages.Packages[i].Forceid
				packagejson.Compresult = comparison.Content.Packages.Packages[i].Compresult
				packagejson.Supersede = comparison.Content.Packages.Packages[i].Supersede
				packagejson.Unselectedreason = comparison.Content.Packages.Packages[i].Unselectedreason
				packagejson.Flashorder = comparison.Content.Packages.Packages[i].Flashorder
				packagejson.Agentlesssuport = comparison.Content.Packages.Packages[i].Agentlesssuport

				if len(comparison.Content.Packages.Packages[i].Childupdates.Childupdates) > 0 {
					packagejson.Childupdates = nil
					for j := 0; j < len(comparison.Content.Packages.Packages[i].Childupdates.Childupdates); j++ {
						childupdatejson.Cadaptername	= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cadaptername
						childupdatejson.Cname 			= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cname
						childupdatejson.Ccategory 		= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Ccategory
						childupdatejson.Cinstanceid 	= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cinstanceid
						childupdatejson.Cnewversion 	= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cnewversion
						childupdatejson.Cinstalled 		= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cinstalled
						childupdatejson.Cdescription 	= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cdescription
						childupdatejson.Cslotnum 		= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cslotnum
						childupdatejson.Cslottype 		= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cslottype
						childupdatejson.Cclassification = comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cclassification
						childupdatejson.Ccompareresult 	= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Ccompareresult
						childupdatejson.Cselected 		= comparison.Content.Packages.Packages[i].Childupdates.Childupdates[j].Cselected
			
						packagejson.Childupdates = append (packagejson.Childupdates,childupdatejson)
					}
				}
				contentjson.Packages = append (contentjson.Packages,packagejson)

			}
			comparisonjson.Content = contentjson
			rootjson.Comparison = comparisonjson
		}
	} else {
		rootjson.Error = "Yes"
		rootjson.Message = "UUID not found or action not in status 'Completed'."
		rootjson.Logfile = filename
	}
	json.NewEncoder(w).Encode(rootjson)
}

//
// func handleRequests is the HTTP request handler and refers to the corresponding functions
//
func handleRequests(portint int, oneclidir string, pooldir string, logsdir string, simulator bool, username string, password string) {

	var err error
/*
	flash:
	./onecli update flash --bmc USERID:<pass>@<ip of ipmi> --dir pool --includeid lnvgy_fw_lxpm_pdl130j-2.02_anyos_noarch <updateID
	./onecli update flash --bmc USERID:<pass>@<ip of ipmi> --dir pool --nocompare --includeid lnvgy_fw_lxpm_pdl130j-2.02_anyos_noarch <updateID


	reboot:
	OneCli.exe misc rebootbmc --bmc userid:password@host[:port]
	
	syshealth:
	OneCli.exe misc syshealth --device system --bmc userid:password@host
*/	
	var onecliapi OnecliApi
		
	err = nil
	
	onecliapi.oneclidir = oneclidir
	onecliapi.pooldir = pooldir
	onecliapi.logsdir = logsdir
	onecliapi.simulator = simulator
	onecliapi.username = username
	onecliapi.password = password
	onecliapi.db = nil

    log.Printf("Rest API v2.0 - Mux Routers at (tls) port %d\n\n",portint)
	if simulator {
		log.Printf("Simulator mode enabled !!!\n\n")
	}
	
	onecliapi.opendb ()
	defer onecliapi.closedb()	
	
	onecliapi.loadexamples ()
	
	err = onecliapi.generateCertAndKey () // initates necliapi.certfile and necliapi.keyfile
	if err != nil {
		log.Fatal(err)
	}
	
	if username == "" || password == "" {
		err = onecliapi.generateDefaultUserAndPassword ()
		if err != nil {
			log.Fatal(err)
		}
	} else {
		onecliapi.username = username
		onecliapi.password = password
	}

	go onecliapi.executeOnecli()

    // creates a new instance of a mux router
    onecliRouter := mux.NewRouter().StrictSlash(true)
	
    onecliRouter.HandleFunc("/", onecliapi.homePage)
    onecliRouter.HandleFunc("/actions", onecliapi.getAllOnecliActionsHandler)
	onecliRouter.HandleFunc("/action", onecliapi.addOnecliActionHandler).Methods("POST")
	onecliRouter.HandleFunc("/action/{uuid}", onecliapi.deleteOnecliActionHandler).Methods("DELETE")
	onecliRouter.HandleFunc("/action/{uuid}", onecliapi.updateOnecliActionHandler).Methods("PATCH")
	onecliRouter.HandleFunc("/action/{uuid}", onecliapi.getOnecliActionHandler).Methods("GET")
	onecliRouter.HandleFunc("/xml-common-result/{uuid}", onecliapi.getOnecliXmlcommonresultHandler).Methods("GET")
	onecliRouter.HandleFunc("/xml-update-compare/{uuid}", onecliapi.getOnecliXmlupdatecompareHandler).Methods("GET")
	onecliRouter.HandleFunc("/actions-ip/{ip}", onecliapi.getOnecliActionsIpHandler).Methods("GET")
    onecliRouter.HandleFunc("/status", onecliapi.getStatusOnecliHandler).Methods("GET")
    onecliRouter.HandleFunc("/updateacquire", onecliapi.getOnecliUpdateAcquireHandler).Methods("GET")
	
	// Create a CA certificate pool and add tls.crt to it
	caCert, err := ioutil.ReadFile(onecliapi.certfile)
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// Create the TLS Config with the CA pool and enable Client certificate validation
	tlsConfig := &tls.Config{
		ClientCAs: caCertPool,
		ClientAuth: tls.RequireAndVerifyClientCert,
		//
		MinVersion:               tls.VersionTLS13,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,  	// to prevent error http2: TLSConfig.CipherSuites is missing an HTTP/2-required AES_128_GCM_SHA256 cipher
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,	// to prevent error http2: TLSConfig.CipherSuites is missing an HTTP/2-required AES_128_GCM_SHA256 cipher
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},				
		//
		
	}
	tlsConfig.BuildNameToCertificate()

	port := fmt.Sprintf (":%d",portint)

	// Create a Server instance to listen on port 8443 with the TLS config
	server := &http.Server{
		//Addr:      ":8443",
		Addr:      port,
		TLSConfig: tlsConfig,
		Handler:   onecliRouter,
	}	

	
	// Listen to HTTPS connections with the server certificate and wait
	//log.Fatal(server.ListenAndServeTLS("tls.crt", "tls.key"))	
	log.Fatal(server.ListenAndServeTLS(onecliapi.certfile, onecliapi.keyfile))	
}

//
// func checkPaths if some folders in the filesystem exist
//
func checkPath (oneclidir string, pooldir string, logsdir string, simulator bool) error {

	var err error
	var simulatordir string
	var filename string
	
	if _, err = os.Stat(pooldir); os.IsNotExist(err) {
		err = os.Mkdir(pooldir, os.ModeDir|0755)
	}
	if err != nil {
		log.Println("Error in func checkPath: could not create pool directory")
		return err
	}

	if _, err = os.Stat(logsdir); os.IsNotExist(err) {
		err = os.Mkdir(pooldir, os.ModeDir|0755)
	}
	if err != nil {
		log.Println("Error in func checkPath: could not create logs directory")
		return err
	}
	
	if !simulator {
		filename = fmt.Sprintf ("%s/onecli",oneclidir)
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Println("Error in func checkPath: onecli definitely does not exist.")
			// this would be very strange
			if err == nil {
				err = errors.New("Error in func checkPath: exit code != 0 but err == nil")
			}
			return err
		}
	} else {
		simulatordir = "/simulator"
		filename = fmt.Sprintf ("%s/common_result.xml",simulatordir)
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Println("Error in func checkPath: file common_result.xml definitely does not exist.")
			// this would be very strange
			if err == nil {
				err = errors.New("Error in func checkPath: exit code != 0 but err == nil")
			}
			return err
		}
		filename = fmt.Sprintf ("%s/Onecli-update-compare.xml",simulatordir)
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Println("Error in func checkPath: file Onecli-update-compare.xml definitely does not exist.")
			// this would be very strange
			if err == nil {
				err = errors.New("Error in func checkPath: exit code != 0 but err == nil")
			}
			return err
		}
		filename = fmt.Sprintf ("%s/Onecli-update-query.xml",simulatordir)
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Println("Error in func checkPath: file Onecli-update-query.xml definitely does not exist.")
			// this would be very strange
			if err == nil {
				err = errors.New("Error in func checkPath: exit code != 0 but err == nil")
			}
			return err
		}
		filename = fmt.Sprintf ("%s/Onecli-update-scan.xml",simulatordir)
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Println("Error in func checkPath: file Onecli-update-scan.xml definitely does not exist.")
			// this would be very strange
			if err == nil {
				err = errors.New("Error in func checkPath: exit code != 0 but err == nil")
			}
			return err
		}
	}

	return err
}

//
// func print_examples supports the -e parameter
//
func print_examples () {

var examples = `

GET:
	status:
    curl -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/status

    all actions:
    curl -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/actions
	
    select action by UUID:
    curl -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/action/6ba7b899-9dad-11d1-80b4-00c04fd430c8 | jq
	
    content of file common_result.xml:
    curl -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/xml-comon-result/6ba7b899-9dad-11d1-80b4-00c04fd430c8 | jq
	
    contet of file Onecli-update-compare.xml:
    curl -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/xml-update-compare/6ba7b899-9dad-11d1-80b4-00c04fd430c8 | jq

    get all actions onecli update acquire (Machine Types):
    curl -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/updateacquire | jq


POST:
    onecli update aquire:
    curl -u user:password -d '{"Command": "update", "Subcommand": "acquire", "MachineType": "7x02", "OsType": "none", "Scope": "latest" }' -H "Content-Type: application/json" -X POST --cert ./tls.crt --key ./tls.key --cacert ./tls.crt  https://onecli-svc:8443/action
	
    onecli update compare:
    curl -u user:password -d '{"Command": "update", "Subcommand": "compare", "User": "xxxxx", "Password": "xxxxx", "IP": "192.168.0.10" }' -H "Content-Type: application/json" -X POST --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/action

    onecli update flash:
    curl -u user:password -d '{"Command": "update", "Subcommand": "flash", "User": "xxxxx", "Password": "xxxxx", "IP": "192.168.0.10", "Updateid": "lnvgy_fw_lxpm_pdl130j-2.02_anyos_noarch" }' -H "Content-Type: application/json" -X POST --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/action

PATCH:
    change action:
    curl -u user:password -d '{"MachineType": "7x02", "OsType": "none", "Scope": "latest" }' -H "Content-Type: application/json" -X PATCH --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/action/6ba7b899-9dad-11d1-80b4-00c04fd430c8

DELETE:
    delete action:
    curl -X DELETE -u user:password --cert ./tls.crt --key ./tls.key --cacert ./tls.crt https://onecli-svc:8443/action/4f282fb1-193e-4662-a89c-76e7e8d4869a
	
`
	fmt.Print(examples)
}

//
// main function
//
func main() {

    var arg_port string
	var arg_oneclidir string
    var arg_pooldir string
	var arg_logsdir string
	var arg_examples bool
	var arg_simulator bool
    var arg_userpassword string
	
	var tmp []string
	var username string
	var password string

    flag.StringVar(&arg_port, 			"p", "8443", "port")
    flag.StringVar(&arg_oneclidir, 		"o", "/oneclidir", "onecli directory")
    flag.StringVar(&arg_pooldir,	 	"d", "/oneclidir/pool", "pool directory")
    flag.StringVar(&arg_logsdir, 		"l", "/oneclidir/logs", "logs directory")
    flag.BoolVar(&arg_examples, 		"e", false, "[true|false] print examples")
    flag.BoolVar(&arg_simulator, 		"s", false, "[true|false] simulator mode")
    flag.StringVar(&arg_userpassword,	"u", "", "user:password")
    flag.Parse()
	
	portint, err := strconv.Atoi(arg_port)
    if err != nil {
        // handle error
		portint = 8443
    }
	
	if len(arg_userpassword) != 0 {
		tmp = strings.Split(arg_userpassword, ":")
		if len(tmp) != 2 {
			log.Printf ("Wrong Username/Password")
			os.Exit(1)
		}
		username = tmp[0]
		password = tmp[1]
	} else {
		username = ""
		password = ""
	}
	
	err = checkPath (arg_oneclidir, arg_pooldir, arg_logsdir, arg_simulator)
	if err != nil {
		os.Exit(1)
	}
	
	if arg_examples {
		print_examples ()
		os.Exit(0)
	}
	
    handleRequests(portint, arg_oneclidir, arg_pooldir, arg_logsdir, arg_simulator, username, password)
}




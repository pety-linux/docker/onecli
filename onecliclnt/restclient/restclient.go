//
// v1.22.0.2021.04.30
//
package restclient

/* list of functions

CheckConnectionHostPort(host string, port string)

PrintReadableJson (jsonData []byte) error

RestDeleteAction (host string, port string, uuid string) (OnecliAction, error)

RestGetAction (host string, port string, uuid string) (OnecliAction, error)
RestGetActions (host string, port string) ([]OnecliAction, error)
RestGetActionsIp (host string, port string, ip string) ([]OnecliAction, error)
RestGetStatus (host string, port string) (OnecliStatus, error)
RestGetUpdateAcquire (host string, port string) ([]OnecliAction, error)
RestGetXmlCommonResult (host string, port string, uuid string) (CommonResultJson, error)
RestGetXmlUpdateCompare (host string, port string, uuid string) (UpdateCompareJson, error)

RestPatchAction (host string, port string, uuid string, requestObject OnecliAction) (OnecliAction, error)

RestPostAction (host string, port string, requestObject OnecliAction) (OnecliAction, error)

RestStatusOk (host string, port string) (bool, error)

 */

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"	
    "crypto/sha256"
    "encoding/hex"	
	"encoding/json"
	"fmt"
	"io/ioutil"	
	"net"
    "net/http"
	"time"
	
	guuid "github.com/google/uuid"	
)

type OnecliAction struct {
    UUID 			string `json:"uuid"`
	Command 		string `json:"command"`
	Subcommand 		string `json:"subcommand"`
	MachineType 	string `json:"machinetype"`
	OsType 			string `json:"ostype"`
	Scope 			string `json:"scope"`
	User 			string `json:"user"`
	Password		string `json:"password"`
	IP	 			string `json:"ip"`
	Updateid		string `json:"updateid"`
	RebootAllowed	string `json:"rebootallowed"`
	Status 			string `json:"status"`
	Timestamp		string `json:"timestamp"`
}

/*
 * JSON struct of file actions pipeline (needed for GET, POST, PATCH)
 */
type OnecliStatus struct {
	Status 			string `json:"status"`
    Created 		string `json:"created"`
	Pending	 		string `json:"pending"`
	Running 		string `json:"running"`
	Failed 			string `json:"failed"`
	Completed	 	string `json:"completed"`
	All 			string `json:"all"`
}


/*
 * JSON struct of file common_result.xml
 */
type CommonResultJson struct {
	Modules			ModulesJson	`json:"modules"`
	Error			string		`json:"error"`
	Message			string		`json:"message"`
	Onecli			string		`json:"onecli"`
	Logfile			string		`json:"logfile"`
}

type ModulesJson struct {
	Modules			[]ModuleJson	`json:"module"`
}

type ModuleJson struct {
	Name			string		`json:"name"`
	Returncode		string		`json:"returncode"`
	Errormessage	string		`json:"errormessage"`
}

// JSON struct of file Onecli-update-compare.xml
type UpdateCompareJson struct {
	Error			string		`json:"ERROR,omitempty"`
	Message			string		`json:"MESSAGE,omitempty"`
	Logfile			string		`json:"LOGFILE,omitempty"`
	Comparison		ComparisonJson	`json:"COMPARISON,omitempty"`	
}

type ComparisonJson struct {
	Content			ContentJson	`json:"CONTENT,omitempty"`
}

type ContentJson struct {
	Mt				string		`json:"MT,omitempty"`
	Bmctype			string		`json:"BMCTYPE,omitempty"`
	Os				string		`json:"OS,omitempty"`
	Arch			string		`json:"ARCH,omitempty"`
	Scope			string		`json:"SCOPE,omitempty"`
	Type			string		`json:"TYPE,omitempty"`
	Directory		string		`json:"DIRETORY,omitempty"`
	Ispartition		string		`json:"ISPARTITION,omitempty"`
	Total			string		`json:"TOTAL,omitempty"`
	Packages		[]PackageJson `json:"PACKAGES,omitempty"`
}

type PackageJson struct {
	Name			string		`json:"NAME,omitempty"`
	Component		string		`json:"COMPONENT,omitempty"`
	Severity		string		`json:"SEVERITY,omitempty"`
	Severitydes		string		`json:"SEVERITYDES,omitempty"`
	Reboot			string		`json:"REBOOT,omitempty"`
	Rebootdes		string		`json:"REBOOTDES,omitempty"`
	Updateid		string		`json:"UPDATEID,omitempty"`
	Pkgversion		string		`json:"PKGVERSION,omitempty"`
	Requisite		string		`json:"REQUISITE,omitempty"`
	Prerequisite	string		`json:"PREREQUISITE,omitempty"`
	Corequisite		string		`json:"COREQUISITE,omitempty"`
	Packagexml		string		`json:"PACKAGEXML,omitempty"`
	Uxspxml			string		`json:"UXSPXML,omitempty"`
	Payloadfile		string		`json:"PAYLOADFILE,omitempty"`
	Commandline		string		`json:"COMMANDLINE,omitempty"`
	// +nullable	
	Childupdates	[]ChildupdateJson `json:"CHILDUPDATES,omitempty"`
	Cinstalled		string		`json:"CINSTALLED,omitempty"`
	Alsoselect		string		`json:"ALSOSELECT,omitempty"`
	Alsodesselect	string		`json:"ALSODESELECT,omitempty"`
	Selected		string		`json:"SELECTED,omitempty"`
	Forceid			string		`json:"FORCEID,omitempty"`
	Compresult		string		`json:"COMPRESULT,omitempty"`
	Supersede		string		`json:"SUPERSEDE,omitempty"`
	Unselectedreason string		`json:"UNSELECTEDREASON,omitempty"`
	Flashorder		string		`json:"FLASHORDER,omitempty"`
	Agentlesssuport	string		`json:"AGENTLESSSUPPORT,omitempty"`
}

type ChildupdateJson struct {
	Cadaptername	string		`json:"CADAPTERNAME,omitempty"`
	Cname			string		`json:"CNAME,omitempty"`
	Ccategory		string		`json:"CCATEGORY,omitempty"`
	Cinstanceid		string		`json:"CINSTANCEID,omitempty"`
	Cnewversion		string		`json:"CNEWVERSION,omitempty"`
	Cinstalled		string		`json:"CINSTALLED,omitempty"`
	Cdescription	string		`json:"CDESCRIPTION,omitempty"`
	Cslotnum		string		`json:"CSLOTNUM,omitempty"`
	Cslottype		string		`json:"CSLOTTYPE,omitempty"`
	Cclassification	string		`json:"CCLASSIFICATION,omitempty"`
	Ccompareresult	string		`json:"CCOMPARERESULT,omitempty"`
	Cselected		string		`json:"CSELECTED,omitempty"`
}

var client *http.Client
var TLSConfig *tls.Config
var username string
var password string
var isinit bool

func Init () {

	isinit = false
}

func RestInitFile (user string, pw string) error {

	var err error 
	
	err = nil
	
	if isinit {
		return err
	}

	username = user
	password = pw
	
	// Read the key pair to create certificate
	cert, err := tls.LoadX509KeyPair("tls.crt", "tls.key")
	if err != nil {
		return err
	}

	// Create a CA certificate pool and add tls.crt to it
	caCert, err := ioutil.ReadFile("tls.crt")
	if err != nil {
		return err
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	
	TLSConfig = &tls.Config{
				RootCAs: caCertPool,
				Certificates: []tls.Certificate{cert},
				//
				MinVersion:               tls.VersionTLS13,
				CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
				PreferServerCipherSuites: true,
				CipherSuites: []uint16{
					tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
					tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_RSA_WITH_AES_256_CBC_SHA,
				},				
				//
			}

	// Create a HTTPS client and supply the created CA pool and certificate
	client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: TLSConfig,
		},
	}
	
	if username == "" || password == "" {
		keyPem, err := ioutil.ReadFile("tls.key")
		if err != nil {
			return err
		}
		generateDefaultUserAndPassword (caCert, keyPem) // caCert => tls.crt
	}
	
	isinit = true
	
	return err
}

func RestInitMemory (user string, pw string, certPem []byte, keyPem []byte) error {

	var err error 
	
	err = nil
	
	if isinit {
		return err
	}

	username = user
	password = pw
	
	// Read the key pair to create certificate
	cert, err := tls.X509KeyPair(certPem, keyPem)
	if err != nil {
		return err
	}

	// Create a CA certificate pool and add tls.crt to it
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(certPem)
	
	TLSConfig = &tls.Config{
				RootCAs: caCertPool,
				Certificates: []tls.Certificate{cert},
				//
				MinVersion:               tls.VersionTLS13,
				CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
				PreferServerCipherSuites: true,
				CipherSuites: []uint16{
					tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
					tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_RSA_WITH_AES_256_CBC_SHA,
				},				
				//
			}

	// Create a HTTPS client and supply the created CA pool and certificate
	client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: TLSConfig,
		},
	}
	
	if username == "" || password == "" {
		generateDefaultUserAndPassword (certPem, keyPem)
	}

	isinit = true
	
	return err
}

func getHash(text []byte) string {

    hasher := sha256.New()
    hasher.Write(text)
    return hex.EncodeToString(hasher.Sum(nil))
}

func generateDefaultUserAndPassword (cert []byte,key []byte) {

	username = getHash (cert)
	password = getHash (key)	
}


func SetUserPassword (user string, pw string) {

	username = user
	password = pw
}

func PrintReadableJson (jsonData []byte) error {

	var err error
	
	err = nil

	jsonDataIndent := &bytes.Buffer{}

	err = json.Indent(jsonDataIndent, jsonData, "", "  ")
	if err != nil {
		return err
	}

	jsonstring := jsonDataIndent.String()

	fmt.Printf ("\n%s\n\n",jsonstring)
	
	return err
}

func CheckConnectionHostPort(host string, port string) error {

	var err error
	
	err = nil
	
	if host == "" || port == "" {
		err = fmt.Errorf("Error in func checkConnectionHostPort: host %s or port %s empty.",host,port)
		return err
	}	

	timeout := time.Duration(1 * time.Second)
	_, err = net.DialTimeout("tcp", host+":"+port, timeout)
	if err != nil {
		return err
	}
	return err
}

func CheckTLSConnection(host string, port string) error {

	var err error
	
	err = nil
	
	if host == "" || port == "" {
		err = fmt.Errorf("Error in func checkConnectionHostPort: host %s or port %s empty.",host,port)
		return err
	}	

	conn, err := tls.Dial("tcp", host+":"+port, TLSConfig)
	if err != nil {
		return err
	}
	conn.Close()
	
	return err
}

/*

https://golang.org/src/net/http/method.go

const (
	MethodGet     = "GET"
	MethodHead    = "HEAD"
	MethodPost    = "POST"
	MethodPut     = "PUT"
	MethodPatch   = "PATCH" // RFC 5789
	MethodDelete  = "DELETE"
	MethodConnect = "CONNECT"
	MethodOptions = "OPTIONS"
	MethodTrace   = "TRACE"
)

*/
func restget (url string) ([]byte, error) {

	var bodyBytes []byte
	var err error
		
	err = nil
	bodyBytes = nil
	
	if url == ""{
		err = fmt.Errorf("Error in func restcall: url empty")
		return bodyBytes, err	
	}
	
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return bodyBytes, err
	}
	req.SetBasicAuth(username, password)
    req.Header.Set("Accept", "application/json")
    //req.Header.Set("Content-Type", "application/json")	
	//req.Header.Set("Content-Type", "application/json; charset=utf-8")
	
	response, err := client.Do(req)
	if err != nil {
		return bodyBytes, err
	}
	defer response.Body.Close()
	
    bodyBytes, err = ioutil.ReadAll(response.Body)
    if err != nil {
		return bodyBytes, err
    }	
	if response.StatusCode != 200 {
		err = fmt.Errorf ("Statuscode: %s",response.Status)
	}
		
	return bodyBytes, err
}

func restpost(url string,jsonReq []byte) ([]byte, error) {

	var bodyBytes []byte
	var err error
		
	err = nil
	bodyBytes = nil
	
	if url == ""{
		err = fmt.Errorf("Error in func restcall: url empty")
		return bodyBytes, err	
	}
	
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonReq))
	if err != nil {
		return bodyBytes, err
	}
	req.SetBasicAuth(username, password)
    req.Header.Set("Accept", "application/json")
    //req.Header.Set("Content-Type", "application/json")	
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	
	response, err := client.Do(req)
	if err != nil {
		return bodyBytes, err
	}
	defer response.Body.Close()
	
    bodyBytes, err = ioutil.ReadAll(response.Body)
    if err != nil {
		return bodyBytes, err
    }	
	if response.StatusCode != 200 {
		err = fmt.Errorf ("Statuscode: %s",response.Status)
	}
		
	return bodyBytes, err
}

func restpatch(url string,jsonReq []byte) ([]byte, error) {

	var bodyBytes []byte
	var err error
		
	err = nil
	bodyBytes = nil
	
	if url == ""{
		err = fmt.Errorf("Error in func restcall: url empty")
		return bodyBytes, err	
	}
	
	req, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(jsonReq))
	if err != nil {
		return bodyBytes, err
	}
	req.SetBasicAuth(username, password)
    req.Header.Set("Accept", "application/json")
    //req.Header.Set("Content-Type", "application/json")	
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	
	response, err := client.Do(req)
	if err != nil {
		return bodyBytes, err
	}
	defer response.Body.Close()
	
    bodyBytes, err = ioutil.ReadAll(response.Body)
    if err != nil {
		return bodyBytes, err
    }	
	if response.StatusCode != 200 {
		err = fmt.Errorf ("Statuscode: %s",response.Status)
	}
		
	return bodyBytes, err
}

func restdelete (url string) ([]byte, error) {

	var bodyBytes []byte
	var err error
		
	err = nil
	bodyBytes = nil
	
	if url == ""{
		err = fmt.Errorf("Error in func restcall: url empty")
		return bodyBytes, err	
	}
	
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return bodyBytes, err
	}
	req.SetBasicAuth(username, password)
    req.Header.Set("Accept", "application/json")
    //req.Header.Set("Content-Type", "application/json")	
	//req.Header.Set("Content-Type", "application/json; charset=utf-8")
	
	response, err := client.Do(req)
	if err != nil {
		return bodyBytes, err
	}
	defer response.Body.Close()
	
    bodyBytes, err = ioutil.ReadAll(response.Body)
    if err != nil {
		return bodyBytes, err
    }	
	if response.StatusCode != 200 {
		err = fmt.Errorf ("Statuscode: %s",response.Status)
	}
		
	return bodyBytes, err
}

func RestStatusOk (host string, port string) (bool, error) {

	var boolstatus bool
	var err error
	var oneclistatus OnecliStatus

	err = nil
	boolstatus = true
	
	if host == "" || port == "" {
		err = fmt.Errorf("Error in func rest_status_ok: host %s or port %s empty.",host,port)
		boolstatus = false
		return boolstatus, err		
	}		
	
	oneclistatus, err = RestGetStatus (host, port)
	
	if oneclistatus.Status != "ok" {
		boolstatus = false
		return boolstatus, err
	}	
	return boolstatus, err
	
}

func RestGetActions (host string, port string) ([]OnecliAction, error) {

	var bodyBytes		[]byte
	var err				error
	var responseObject	[]OnecliAction
	var	url				string

	err = nil
	responseObject = nil

	if host == "" || port == "" {
		err = fmt.Errorf("Error in func RestGetActions: host %s or port %s empty.",host,port)
		return responseObject, err		
	}		

	url = fmt.Sprintf ("https://%s:%s/actions",host,port)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	return responseObject, err		
}

func RestGetActionsIp (host string, port string, ip string) ([]OnecliAction, error) {

	var bodyBytes		[]byte
	var err				error
	var responseObject	[]OnecliAction
	var	url				string

	err = nil
	responseObject = nil

	if host == "" || port == "" || ip == "" {
		err = fmt.Errorf("Error in func RestGetActionsIp: host %s or port %s or IP empty.",host,port,ip)
		return responseObject, err		
	}		

	url = fmt.Sprintf ("https://%s:%s/actions-ip/%s",host,port,ip)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	return responseObject, err		
}

func RestGetAction (host string, port string, uuid string) (OnecliAction, error) {

	var err error
	var responseObject OnecliAction
	var bodyBytes	[]byte	
	var url string
	
	err = nil
	
	if host == "" || port == "" || uuid == "" {
		err = fmt.Errorf("Error in func rest_get_action: host %s or port %s or uuid %s empty.",host,port,uuid)
		return responseObject,err
	}		

	url = fmt.Sprintf ("https://%s:%s/action/%s",host,port,uuid)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	
	return responseObject, err
}

func RestGetXmlCommonResult (host string, port string, uuid string) (CommonResultJson, error) {

	var err error
	var responseObject CommonResultJson
	var bodyBytes	[]byte	
	var url string
	
	err = nil
	
	if host == "" || port == "" || uuid == "" {
		err = fmt.Errorf("Error in func rest_get_xml_common_result: host %s or port %s or uuid %s empty.",host,port,uuid)
		return responseObject, err
	}		

	url = fmt.Sprintf ("https://%s:%s/xml-common-result/%s",host,port,uuid)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	
	return responseObject, err
}

func RestGetXmlUpdateCompare (host string, port string, uuid string) (UpdateCompareJson, error) {

	var err error
	var responseObject UpdateCompareJson
	var bodyBytes	[]byte	
	var url string
	
	err = nil
	
	if host == "" || port == "" || uuid == "" {
		err = fmt.Errorf("Error in func rest_get_xml_update_compare: host %s or port %s or uuid %s empty.",host,port,uuid)
		return responseObject, err
	}		

	url = fmt.Sprintf ("https://%s:%s/xml-update-compare/%s",host,port,uuid)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	
	return responseObject, err
}

func RestGetStatus (host string, port string) (OnecliStatus, error) {

	var err error
	var responseObject	OnecliStatus
	var bodyBytes	[]byte	
	var url string

	err = nil
	
	if host == "" || port == "" {
		err = fmt.Errorf("Error in func rest_get_status: host %s or port %s empty.",host,port)
		return responseObject, err
	}		

	url = fmt.Sprintf ("https://%s:%s/status",host,port)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	
	return responseObject, err
}


func RestGetUpdateAcquire (host string, port string) ([]OnecliAction, error) {

	var bodyBytes		[]byte
	var err				error
	var responseObject	[]OnecliAction
	var	url				string

	err = nil
	responseObject = nil

	if host == "" || port == "" {
		err = fmt.Errorf("Error in func rest_get_update_acquire: host %s or port %s empty.",host,port)
		return responseObject, err		
	}		

	url = fmt.Sprintf ("https://%s:%s/updateacquire",host,port)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	return responseObject, err	
}

func RestPatchAction (host string, port string, uuid string, requestObject OnecliAction) (OnecliAction, error) {

	var bodyBytes		[]byte
	var err 			error
	var jsonData		[]byte
	var responseObject 	OnecliAction
	var url				string

	err = nil
	
	if host == "" || port == "" || uuid == "" {
		err = fmt.Errorf("Error in func rest_patch_action: host %s or port %s or uuid %s empty.",host,port,uuid)
		return responseObject, err
	}		

	url = fmt.Sprintf ("https://%s:%s/action/%s",host,port,uuid)
	
	jsonData, err = json.Marshal(requestObject)
	if err != nil {
		return responseObject, err
	}
	bodyBytes, err = restpatch (url,jsonData)
	if err != nil {
		return responseObject, err	
	}
	json.Unmarshal(bodyBytes, &responseObject)
	
	return responseObject, err
}

func RestPostAction (host string, port string, requestObject OnecliAction) (OnecliAction, error) {
	
	var bodyBytes		[]byte
	var err 			error
	var jsonData		[]byte
	var responseObject 	OnecliAction
	var url				string
	var uuid 			string
	
	err = nil
	
	if host == "" || port == "" {
		err = fmt.Errorf("Error in func rest_post_action: host %s or port %s empty.",host,port)
		return responseObject, err		
	}		

	if requestObject.UUID == "" {
		requestObject.UUID = uuid
		google_uuid := guuid.New()
		uuid = google_uuid.String()
	}

	url = fmt.Sprintf ("https://%s:%s/action",host,port)
	
			
	jsonData, err = json.Marshal(requestObject)
	if err != nil {
		return responseObject, err
	}
	bodyBytes, err = restpost (url,jsonData)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
		
	return responseObject, err
}

func RestDeleteAction (host string, port string, uuid string) (OnecliAction, error) {

	var err error
	var responseObject OnecliAction
	var bodyBytes	[]byte	
	var url string
	
	err = nil
	
	if host == "" || port == "" || uuid == "" {
		err = fmt.Errorf("Error in func rest_delete_action: host %s or port %s or uuid %s empty.",host,port,uuid)
		return responseObject,err
	}		

	url = fmt.Sprintf ("https://%s:%s/action/%s",host,port,uuid)
	bodyBytes, err = restget (url)
	if err != nil {
		return responseObject, err
	}
	json.Unmarshal(bodyBytes, &responseObject)
	
	_, err = restdelete (url)
	if err != nil {
		return responseObject, err
	}
	
	return responseObject, err
}

// HandleFunc fomr REST API server
// onecliRouter.HandleFunc("/", onecliapi.homePage)
// onecliRouter.HandleFunc("/actions", onecliapi.getAllOnecliActionsHandler)
// onecliRouter.HandleFunc("/action", onecliapi.addOnecliActionHandler).Methods("POST")
// onecliRouter.HandleFunc("/action/{uuid}", onecliapi.deleteOnecliActionHandler).Methods("DELETE")
// onecliRouter.HandleFunc("/action/{uuid}", onecliapi.updateOnecliActionHandler).Methods("PATCH")
// onecliRouter.HandleFunc("/action/{uuid}", onecliapi.getOnecliActionHandler).Methods("GET")
// onecliRouter.HandleFunc("/xml-common-result/{uuid}", onecliapi.getOnecliXmlcommonresultHandler).Methods("GET")
// onecliRouter.HandleFunc("/xml-update-compare/{uuid}", onecliapi.getOnecliXmlupdatecompareHandler).Methods("GET")
// onecliRouter.HandleFunc("/actions-ip/{ip}", onecliapi.getOnecliActionsIpHandler).Methods("GET")
// onecliRouter.HandleFunc("/status", onecliapi.getStatusOnecliHandler).Methods("GET")
// onecliRouter.HandleFunc("/updateacquire", onecliapi.getOnecliUpdateAcquireHandler).Methods("GET")

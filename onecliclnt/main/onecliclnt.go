//
// v1.22.0.2021.04.30
//
package main

import (
	"encoding/json"
	"flag"	
	"fmt"
	//"io/ioutil"
	"log"
	//"net/http"	
	"os"
	"strings"
	
	restclient "github.com/onmetal/onecliclnt/restclient"
)

const (
  method_get 	string = "get"
  method_post	string = "post"
  method_patch	string = "patch"
  method_delete	string = "delete"
)

func isFlagPassed(name string) bool {
    found := false
    flag.Visit(func(f *flag.Flag) {
        if f.Name == name {
            found = true
        }
    })
    return found
}

func main() {

	var err 			error
	var crud 			int
	var onecliactions 	[]restclient.OnecliAction
	var onecliaction 	restclient.OnecliAction
	var method 			string

    //var arg_data string
    var arg_data 		string
    var arg_host 		string
	var arg_ip 			string
    var arg_port 		string
	var arg_typ		 	string
	var arg_uuid 		string

	var arg_create 		bool
	var arg_read 		bool
	var arg_update 		bool
	var arg_delete		bool
	
    var arg_userpassword string
	
	var tmp []string
	var username string
	var password string	

	err = nil
	
    flag.StringVar(&arg_data, 	"j", "{ Status: \"Completed\" }", "data (json format)")
    flag.BoolVar(&arg_create,	"c", false, "[true|false] create")
    flag.BoolVar(&arg_read,		"r", false, "[true|false] read")
    flag.BoolVar(&arg_update,	"u", false, "[true|false] update")
    flag.BoolVar(&arg_delete,	"d", false, "[true|false] delete")

    flag.StringVar(&arg_host,	"h", "onecli-svc", "host")
    flag.StringVar(&arg_ip,		"i", "192.168.10.149", "ip")
    flag.StringVar(&arg_port,	"p", "8443", "port")
    flag.StringVar(&arg_typ, 	"t", "actions", "typ (url)")
    flag.StringVar(&arg_uuid,	"s", "0c965d66-25db-4c57-9d91-0d39213ef0eb", "search uuid")
	
    flag.StringVar(&arg_userpassword,	"a", "", "user:password")
	

    flag.Parse()
	
	if len(arg_userpassword) != 0 {
		tmp = strings.Split(arg_userpassword, ":")
		if len(tmp) != 2 {
			err = fmt.Errorf ("Wrong Username/Password")
			log.Fatal (err)
			os.Exit (1)
		}
		username = tmp[0]
		password = tmp[1]	
	} else {
		username = ""
		password = ""
	}
	
	crud = 0
	if arg_create {
		crud++
		method = method_post
	}
	if arg_read {
		crud++
		method = method_get
	}
	if arg_update {
		crud++
		method = method_patch
	}
	if arg_delete {
		crud++
		method = method_delete
	}
	if crud == 0 {
		method = method_get
	} else {
		if crud != 1 {
			err = fmt.Errorf ("needs -c or -r or -u or -d")
			log.Fatal (err)
			os.Exit (1)
		}
	}
	
	if isFlagPassed("s") && !isFlagPassed("t") && !isFlagPassed("c") && !isFlagPassed("u") && !isFlagPassed("d"){
		method = method_get
		arg_typ = "action"
	}
	if isFlagPassed("i") {
		method = method_get
		arg_typ = "actions-ip"
	}
	
//
// Init
//
	err = restclient.RestInitFile (username, password)
	if err != nil {
		log.Fatal(err)
		os.Exit (1)
	}

	err = restclient.CheckTLSConnection(arg_host, arg_port)
	if err != nil {
		log.Fatal(err)
		os.Exit (1)
	}

//
// Get
//
	switch method {
		case method_get:
			switch strings.ToLower(arg_typ) {
				case "actions": 
					// RestGetActions
					log.Println ("RestGetActions")
					onecliactions, err = restclient.RestGetActions (arg_host, arg_port)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
	
					for _, onecliaction = range onecliactions {
						jsonData, err := json.Marshal(onecliaction)
						err = restclient.PrintReadableJson (jsonData)	
						if err != nil {
							log.Fatal(err)
							os.Exit (1)
						}
					}
				case "action":
					// RestGetAction (host string, port string, uuid string) (OnecliAction, error)
					if !isFlagPassed("s") {
						err = fmt.Errorf ("missing option -s")
						log.Fatal (err)
						os.Exit (1)
					}
					log.Println ("RestGetAction")
					onecliaction, err = restclient.RestGetAction (arg_host, arg_port, arg_uuid)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
					jsonData, err := json.Marshal(onecliaction)
					err = restclient.PrintReadableJson (jsonData)	
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
				case "actions-ip": 
					// RestGetActionsIp
					if !isFlagPassed("i") {
						err = fmt.Errorf ("missing option -i")
						log.Fatal (err)
						os.Exit (1)
					}
					log.Println ("RestGetActionsIp")
					onecliactions, err = restclient.RestGetActionsIp (arg_host, arg_port, arg_ip)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
	
					for _, onecliaction = range onecliactions {
						jsonData, err := json.Marshal(onecliaction)
						err = restclient.PrintReadableJson (jsonData)	
						if err != nil {
							log.Fatal(err)
							os.Exit (1)
						}
					}
				case "updateacquire": 
					// RestGetUpdateAcquire (host string, port string) ([]OnecliAction, error)
					log.Println ("RestGetUpdateAcquire")
					onecliactions, err = restclient.RestGetUpdateAcquire (arg_host, arg_port)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
	
					for _, onecliaction = range onecliactions {
						jsonData, err := json.Marshal(onecliaction)
						err = restclient.PrintReadableJson (jsonData)	
						if err != nil {
							log.Fatal(err)
							os.Exit (1)
						}
					}
				case "xml-common-result":
					// RestGetXmlCommonResult (host string, port string, uuid string) (CommonResultJson, error)
					if !isFlagPassed("s") {
						err = fmt.Errorf ("missing option -s")
						log.Fatal (err)
						os.Exit (1)
					}
					log.Println ("RestGetXmlCommonResult")
					commonresultjson, err := restclient.RestGetXmlCommonResult (arg_host, arg_port, arg_uuid)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
					jsonData, err := json.Marshal(commonresultjson)
					err = restclient.PrintReadableJson (jsonData)	
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
				case "xml-update-compare":
					// RestGetXmlUpdateCompare (host string, port string, uuid string) (UpdateCompareJson, error)
					if !isFlagPassed("s") {
						err = fmt.Errorf ("missing option -s")
						log.Fatal (err)
						os.Exit (1)
					}
					log.Println ("RestGetXmlUpdateCompare")
					updatecomparejson, err := restclient.RestGetXmlUpdateCompare (arg_host, arg_port, arg_uuid)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
					jsonData, err := json.Marshal(updatecomparejson)
					err = restclient.PrintReadableJson (jsonData)	
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
				case "status":
					// RestGetStatus (host string, port string) (OnecliStatus, error)
					log.Println ("RestGetStatus")
					oneclistatus, err := restclient.RestGetStatus (arg_host, arg_port)
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
					jsonData, err := json.Marshal(oneclistatus)
					err = restclient.PrintReadableJson (jsonData)	
					if err != nil {
						log.Fatal(err)
						os.Exit (1)
					}
				default:
					log.Fatal ("unknown typ")
					os.Exit (1)
			}
		case method_delete:
			// RestDeleteAction (host string, port string, uuid string) (OnecliAction, error)
			if !isFlagPassed("s") {
				err = fmt.Errorf ("missing option -s")
				log.Fatal (err)
				os.Exit (1)
			}
			log.Println ("RestDeleteAction")
			onecliaction, err = restclient.RestDeleteAction (arg_host, arg_port, arg_uuid)
			if err != nil {
				log.Fatal(err)
				os.Exit (1)
			}
			jsonData, err := json.Marshal(onecliaction)
			err = restclient.PrintReadableJson (jsonData)	
			if err != nil {
				log.Fatal(err)
				os.Exit (1)
			}
		case method_patch:
			// RestPatchAction (host string, port string, uuid string, requestObject OnecliAction) (OnecliAction, error)
			if !isFlagPassed("s") {
				err = fmt.Errorf ("missing option -s")
				log.Fatal (err)
				os.Exit (1)
			}
			if !isFlagPassed("j") {
				err = fmt.Errorf ("missing option -j")
				log.Fatal (err)
				os.Exit (1)
			}
			log.Println ("RestPatchAction")
			json.Unmarshal([]byte(arg_data), &onecliaction)			
			onecliaction, err = restclient.RestPatchAction (arg_host, arg_port, arg_uuid, onecliaction)
			if err != nil {
				log.Fatal(err)
				os.Exit (1)
			}
			jsonData, err := json.Marshal(onecliaction)
			err = restclient.PrintReadableJson (jsonData)	
			if err != nil {
				log.Fatal(err)
				os.Exit (1)
			}
		case method_post:
			// RestPostAction (host string, port string, requestObject OnecliAction) (OnecliAction, error)
			if !isFlagPassed("j") {
				err = fmt.Errorf ("missing option -j")
				log.Fatal (err)
				os.Exit (1)
			}
			log.Println ("RestPostAction")
			json.Unmarshal([]byte(arg_data), &onecliaction)			
			onecliaction, err = restclient.RestPostAction (arg_host, arg_port, onecliaction)
			if err != nil {
				log.Fatal(err)
				os.Exit (1)
			}
			jsonData, err := json.Marshal(onecliaction)
			err = restclient.PrintReadableJson (jsonData)	
			if err != nil {
				log.Fatal(err)
				os.Exit (1)
			}
		default:
			log.Fatal ("unknown method")
			os.Exit (1)
	}
//
// Ende
//	
	os.Exit (0)
}

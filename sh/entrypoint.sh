#!/bin/bash -e
umask 027
echo Start oneclirest v1.22.0.2021.04.30

if [ "$ONECLI_FUNCTION" = "LOOP" ]
then
    while true; do sleep 3600; done
elif [ "$ONECLI_FUNCTION" = "SIMULATOR" ]
then
    cd /oneclidir
	if [ -f "./oneclirest-credentials.json" ]
	then
		echo "oneclirest-credentials.json exists on the filesystem."
		onecliun=$(cat "./oneclirest-credentials.json"|jq .credentials.user|base64 -di)
		oneclipw=$(cat "./oneclirest-credentials.json"|jq .credentials.password|base64 -di)
		onecliunpw=$onecliun":"$oneclipw
	    ./oneclirest -o /oneclidir -d /oneclidir/pool -l /oneclidir/logs -p 8443 -s true -a $onecliunpw
	else	
		./oneclirest -o /oneclidir -d /oneclidir/pool -l /oneclidir/logs -p 8443 -s true
	fi	
else
    cd /oneclidir
	if [ -f "./oneclirest-credentials.json" ]
	then
		echo "oneclirest-credentials.json exists on the filesystem."
		onecliun=$(cat "./oneclirest-credentials.json"|jq .credentials.user|base64 -di)
		oneclipw=$(cat "./oneclirest-credentials.json"|jq .credentials.password|base64 -di)
		onecliunpw=$onecliun":"$oneclipw
	    ./oneclirest -o /oneclidir -d /oneclidir/pool -l /oneclidir/logs -p 8443 -a $onecliunpw
	else	
		./oneclirest -o /oneclidir -d /oneclidir/pool -l /oneclidir/logs -p 8443
	fi	
fi

echo Ende oneclirest

